---
title: "Experiencia"
date: 2022-03-08
layout: "simple"
sharingLinks: false
---

## Trabajo
{{< timeline >}}

{{< timelineItem icon="code" header="Infotec" badge="May 2024 - Present" subheader="Developer" >}}
Mejoras, depuración e implementación de nuevas funcionalidades para la plataforma Rizoma del Consejo Nacional de Humanidades, Ciencias y Tecnologías (CONAHCYT), y atendiendo solicitudes de corrección y actualización de datos.
{{< keywordList >}}
{{< keyword >}} TypeScript {{< /keyword >}}
{{< keyword >}} Vue.js {{< /keyword >}}
{{< keyword >}} Python {{< /keyword >}}
{{< keyword >}} MongoDB {{< /keyword >}}
{{< keyword >}} PostgreSQL {{< /keyword >}}
{{< /keywordList >}}
{{< /timelineItem >}}

{{< timelineItem icon="code" header="Percona" badge="July 2022 - May 2023" subheader="Technical Evangelist" >}}
<ul>
  <li>Technical Writing (<a href="https://percona.com">percona.com</a>/<a href="https://percona.community">percona.community</a>) &rarr; <a href="https://mariog.dev/tags/percona/">Artículos</a></li>
  <li>Documentación Técnica (Mejoras)</li>
  <li>Co/Anfitrión Transmisión en Vivo (MySQL/PostgreSQL) &rarr; <a href="https://percona.community/speakers/mario_garcia/">Grabaciones</a></li>
  <li>Conferencias</li>
</ul>
{{< keywordList >}}
{{< keyword >}} Python {{< /keyword >}}
{{< keyword >}} MySQL {{< /keyword >}}
{{< keyword >}} PostgreSQL {{< /keyword >}}
{{< keyword >}} MongoDB {{< /keyword >}}
{{< keyword >}} Docker {{< /keyword >}}
{{< keyword >}} Kubernetes {{< /keyword >}}
{{< keyword >}} AWS {{< /keyword >}}
{{< /keywordList >}}
{{< /timelineItem >}}

{{< timelineItem icon="code" header="TerminusDB" badge="July 2021 - October 2021" subheader="Intern, Community Team" >}}
Tutorials
<ul>
  <li>Configuración del entorno de desarrollo</li>
  <li>Creación del esquema</li>
  <li>Importar datos a TerminusDB</li>
  <li>Uso del cliente de Python</li>
  <li>Uso de las taps de Singer.io</li>
</ul>
GitHub:
<ul>
  <li><a href="https://github.com/mattdark/terminusdb-tutorials/tree/master/brewery">Brewery</a></li>
  <li><a href="https://github.com/mattdark/terminusdb-tutorials/tree/master/gitlab_data">GitLab</a></li>
  <li><a href="https://github.com/mattdark/terminusdb-tutorials/tree/master/netflix">Netflix</a></li>
  <li><a href="https://github.com/mattdark/terminusdb-tutorials/tree/master/nobel_prize">Nobel Prize</a></li>
</ul>
{{< keyword >}} Python {{< /keyword >}}
{{< /timelineItem >}}
{{< /timeline >}}

## Community Participation
{{< timeline >}}
{{< timelineItem icon="code" header="HashiCorp" badge="2021 - 2023" subheader="HashiCorp Ambassador" >}}
<ul>
  <li>Creación de contenido. Artículos y videos.</li>
  <li>Evangelismo. Conferencista e instructor en conferencias, meetups, y webinars.</li>
</ul>
{{< /timelineItem >}}

{{< timelineItem icon="code" header="GitLab" badge="August 2019 - Ongoing" subheader="GitLab Hero" >}}
<ul>
  <li>Creador de contenido. Artículos y videos.</li>
  <li>Evangelismo. Conferencista e instructor en conferencias, meetups, y webinars.</li>
</ul>
{{< /timelineItem >}}

{{< timelineItem icon="code" header="GitKraken" badge="April 2019 - 2022" subheader="GitKraken Ambassador" >}}
<ul>
  <li>Creador de contenido. Artículos y videos.</li>
  <li>Evangelismo. Conferencista e instructor en conferencias, meetups, y webinars.</li>
</ul>
{{< /timelineItem >}}

{{< timelineItem icon="code" header="Mozilla Foundation" badge="August 2011 - September 2020" subheader="Contributor" >}}
<ul>
  <li>Firefox OS. Equipo de lanzamiento en Mexico (2013 – 2015)</li>
  <li>Localización. Firefox & Webmaker. (2012 – 2013)</li>
  <li>Rust. Conferencista e instructor. (2016 – 2020)</li>
  <li>Open Leaders. Experto invitado y mentor (February 2018 – December 2020)</li>
  <li>Mozilla Reps (August 2017 – August 2020)</li>
</ul>
{{< /timelineItem >}}

{{< /timeline >}}