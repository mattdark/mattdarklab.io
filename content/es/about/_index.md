---
title: "Sobre Mí"
date: 2022-03-08
layout: "simple"
sharingLinks: false
---

# Sobre Mí

## ¡Hola!, Soy Mario García

Desarrollador y conferencista mexicano. He contribuido a proyectos de Software Libre y Código Abierto por más de 15 años, y la mayor parte de mi trabajo/contribuciones han sido en las áreas de localización, evangelización, creación de contenido, y desarrollo de software.

Escribí mis primeras líneas de código cuando cursaba el bachillerato, y desde entonces me interesé por el desarrollo de software. Últimamente me he enfocado en mejorar mis habilidades como desarrollador.

Fui un contribuidor activo de [Mozilla](https://mozilla.org) por más de 9 años. Parte del equipo de lanzamiento de Firefox OS en México. Contribuí en la localización Firefox y Webmaker a español. Promoví el uso de Rust en América Latina.

También tuve la oportunidad de formar parte de [Mozilla Open Leaders](https://foundation.mozilla.org/en/opportunity/mozilla-open-leaders/) como mentor, facilitador y experto invitado de 2018 a 2020.

Debido a mis contribuciones, también me uní a [GitLab Heroes](https://about.gitlab.com/community/heroes/), y [GitKraken Ambassadors](https://www.gitkraken.com/ambassador) en 2019. Y fui reconocido como [HashiCorp Ambassador](https://www.hashicorp.com/ambassadors) por tres años seguidos.

De julio a octubre de 2021, me uní a [TerminusDB](https://terminusdb.com/) como pasante, miembro del equipo de comunidad, donde trabajé en tutoriales escritos cubriendo temas relacionados con configuración del entorno de desarrollo, creación del esquema de la base de datos, uso del cliente de Python, y otras herramientas para importar datos a TerminusDB.

De julio de 2022 a mayo de 2023, me uní a [Percona](https://percona.com) como Evangelista Técnico, miembro del equipo de comunidad, donde escribí artículos sobre temas relacionados con bases de datos, contenedores, desarrollo de software y DevOps, así como ser anfitrión de dos transmisiones en vivo sobre bases de datos, y siendo conferencista en eventos de tecnología.

Soy conferencista con más de 15 años de experiencia, con pasión por educar. He colaborado con organizaciones y comunidades de tecnología de alrededor del mundo, y he participado tanto en eventos presenciales como virtuales.

Como autodidacta, disfruto mucho aprender cosas nuevas. Pasé el último año mejorando mi conocimiento sobre bases de datos, contenedores y Python, y conociendo nuevas tecnologías como Kubernetes, MongoDB y algunas herramientas y servicios de AWS.