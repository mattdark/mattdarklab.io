---
title: "Conferencias"
date: 2024-11-11
layout: "simple"
sharingLinks: false
---

He sido conferencista técnico desde 2008 cuando asistí a mi primer evento, el Festival Latinoamericano de Instalación de Software Libre Celebrado en mi ciudad. Fui invitado a compartir mi experiencia usando GNU/Linux.

A través de los años, he sido representante de diferentes organizaciones como [Mozilla](https://www.mozilla.org/), [GitLab](https://about.gitlab.com/), [GitKraken](https://www.gitkraken.com/), [HashiCorp](https://www.hashicorp.com/) y [Percona](https://www.percona.com/). Compartiendo mi experiencia sobre diferentes temas, incluyendo Código Abierto, desarrollo de software, DevOps, bases de datos y contenedores.

GitLab Commit, PyCon Chile, Pyjamas Conf, HashiTalks, FOSDEM and FOSSCOMM son algunos de los eventos en los que he participado como conferencista, de manera virtual como en persona.

También he sido mentor de otros conferencistas en [PyCon US](https://us.pycon.org/) y [EuroPython](https://www.europython.eu).

A continuación hay una lista de reproducción con algunas grabaciones disponibles, tanto en inglés como en español.

{{< youtubepl id="PLj0IM88rjwPAu7ObZlNsNM6SL9r6u-x4K" >}}

Realmente disfruto compartir conocimiento con la comunidad global. ¿Hay algún evento en el que quieras que participe? Definitivamente diría que sí a eventos virtuales o locales. Estoy disponible para viajar, pero se requieren gastos de viajes.

¿Te gustaría participar en tu primer? ¿Necesitas que alguien te guíe para preparar tu presentación o escribir una propuesta? Más que feliz de ayudar. Disponible para sesiones de mentoría.

También escribir un artículo sobre [Mis Reglas Para Ser Conferencista Técnico](https://mariog.dev/es/blog/mis-reglas-para-ser-conferencista-tecnico/) con mis recomendaciones y todo lo que he aprendido después de ser conferencista por más de 16 años.

{{< button href="mailto:hi@mariog.xyz" target="_self" >}}
Di Hola
{{< /button >}}