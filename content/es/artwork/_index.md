---
title: "Dibujos"
date: 2024-11-18
layout: "simple"
sharingLinks: false
---

# Dibujos

Cada dibujo en esta sección fue creado con [Inkscape](https://inkscape.org/). Publicado con CC BY-NC-SA 4.0. Para ver una copia de esta licencia, visita https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es

{{< gallery >}}
  <img src="https://gitlab.com/mattdark/artwork/-/raw/main/drawings/bat-monster/bat-monster.png" class="grid-w50 md:grid-w33 xl:grid-w25" />
  <img src="https://gitlab.com/mattdark/artwork/-/raw/main/drawings/bat/bat.png" class="grid-w50 md:grid-w33 xl:grid-w25" />
  <img src="https://gitlab.com/mattdark/artwork/-/raw/main/drawings/cat-with-wings/cat-with-wings.png" class="grid-w50 md:grid-w33 xl:grid-w25" />
  <img src="https://gitlab.com/mattdark/artwork/-/raw/main/drawings/cat/cat.png" class="grid-w50 md:grid-w33 xl:grid-w25" />
  <img src="https://gitlab.com/mattdark/artwork/-/raw/main/drawings/couple-cat/couple-cat.png" class="grid-w50 md:grid-w33 xl:grid-w25" />
  <img src="https://gitlab.com/mattdark/artwork/-/raw/main/drawings/friday-the-13th/jason.png" class="grid-w50 md:grid-w33 xl:grid-w25" />
  <img src="https://gitlab.com/mattdark/artwork/-/raw/main/drawings/harry-potter/harry-potter.png" class="grid-w50 md:grid-w33 xl:grid-w25" />
  <img src="https://gitlab.com/mattdark/artwork/-/raw/main/drawings/may-the-source/may-the-source-be-with-you.png" class="grid-w50 md:grid-w33 xl:grid-w25" />
  <img src="https://gitlab.com/mattdark/artwork/-/raw/main/drawings/sunflower/sunflower.png" class="grid-w50 md:grid-w33 xl:grid-w25" />
  <img src="https://gitlab.com/mattdark/artwork/-/raw/main/drawings/sun-moon/sunmoon.png" class="grid-w50 md:grid-w33 xl:grid-w25" />
  <img src="https://gitlab.com/mattdark/artwork/-/raw/main/drawings/swan-monster/swan-monster.png" class="grid-w50 md:grid-w33 xl:grid-w25" />
{{< /gallery >}}

Puedes encontrar los archivos SVG originales en el siguiente repositorio de GitLab. 

{{< gitlab projectID="64635862" >}}