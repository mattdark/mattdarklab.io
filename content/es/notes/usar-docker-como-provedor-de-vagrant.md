---
title: "Usar Docker Como Provedor de Vagrant"
date: 2024-11-10
description: "Cómo configurar almacenamiento de objetos con MinIO usando Docker"
summary: "Aprende a desplegar una instancia de MinIO usando Docker"
tags: ["tutorial", "docker", "linux"]
---

De acuerdo a la [documentación](https://www.vagrantup.com/docs/providers), Vagrant soporta [VirtualBox](https://www.virtualbox.org/), [VMWare](https://www.vmware.com/), [Hyper-V](https://www.microsoft.com/hyper-v) y [Docker](https://www.docker.io/) como provedores.

A través de este artículo aprenderás como configurar Vagrant para usar Docker.

## Docker
Para instalar Docker en Linux sigue las instrucciones en la [documentación](https://docs.docker.com/engine/install/). Ve directamente a las instrucciones de la distribución que usas:

* [CentOS](https://docs.docker.com/engine/install/centos)
* [Debian](https://docs.docker.com/engine/install/debian)
* [Fedora](https://docs.docker.com/engine/install/fedora)
* [Raspbian](https://docs.docker.com/engine/install/debian)
* [Ubuntu](https://docs.docker.com/engine/install/ubuntu)

Si usas Arch Linux o cualquier distribución basada en Arch, instala usando `pacman` e inicializa el proceso:

```bash
$ sudo pacman -S docker
$ sudo systemctl start docker
```

Para ejecutar comandos de Docker necesitarás permisos de `root`. Vagrant se encargará de ejcutar Docker, por ello debes configurarlo para que se ejecute sin usar `sudo`. Puedes seguir las instrucciones en la sección [Post-installation steps for Linux](https://docs.docker.com/engine/install/linux-postinstall/) como se explica a continuación.

Primero crea el grupo `docker`:

```bash
$ sudo groupadd docker
```

Agrega tu usuario al grupo `docker`:

```bash
$ sudo usermod -aG docker $USER
```

Tendrás que cerrar e iniciar sesión para que los cambios surtan efecto.

Si quieres activar los cambios en los grupos para tu actual sesión, ejecuta:

```bash
$ newgrp docker
```

Para verificar que los comandos de Docker pueden ejecutarse sin usar `sudo`:

```
$ docker run hello-world
```

Este comando descargará el contenedor de prueba `hello-world` y lo ejecutará.


## Vagrant
Para instalar Vagrant ve a la [página de descargas](https://www.vagrantup.com/downloads) y obten el paquete correcto para tu distribución. También puedes instalar desde los repositorios de algunas distribuciones GNU/Linux.

#### Debian-based:

```bash
$ sudo apt install vagrant
```

#### Fedora:
 
```bash
$ sudo dnf install vagrant
```

#### CentOS

```bash
$ sudo dnf install -y https://releases.hashicorp.com/vagrant/2.2.9/vagrant_2.2.9_x86_64.rpm
```

#### Arch Linux:

```bash
$ sudo pacman -S vagrant
```

## Vagrant + Docker
Hay dos formas de usar Docker como provedor. Usando una imagen del registro de Docker:

```ruby
Vagrant.configure("2") do |config|
  config.vm.provider "docker" do |d|
    d.image = "foo/bar"
  end
end
```

O un `Dockerfile`:

```ruby
Vagrant.configure("2") do |config|
  config.vm.provider "docker" do |d|
    d.build_dir = "."
  end
end
```

### Usar un Dockerfile
Primero tienes que crear un directorio para almacenar los archivos de configuración del entorno y cambiarte a este directorio.

```bash
$ mkdir docker-test
$ cd docker-test
```

Crear un Dockerfile:

```bash
$ touch Dockerfile
```

Y agrega el siguiente contenido:

```docker                                                                            
FROM ubuntu

ENV TZ=America/Mexico_City
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update -y

RUN apt-get install -y --no-install-recommends ssh sudo

RUN useradd --create-home -s /bin/bash vagrant
RUN echo -n 'vagrant:vagrant' | chpasswd
RUN echo 'vagrant ALL = NOPASSWD: ALL' > /etc/sudoers.d/vagrant
RUN chmod 440 /etc/sudoers.d/vagrant
RUN mkdir -p /home/vagrant/.ssh
RUN chmod 700 /home/vagrant/.ssh
RUN echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ==" > /home/vagrant/.ssh/authorized_keys
RUN chmod 600 /home/vagrant/.ssh/authorized_keys
RUN chown -R vagrant:vagrant /home/vagrant/.ssh
RUN sed -i -e 's/Defaults.*requiretty/#&/' /etc/sudoers
RUN sed -i -e 's/\(UsePAM \)yes/\1 no/' /etc/ssh/sshd_config

RUN mkdir /var/run/sshd

RUN apt-get -y install openssh-client

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]
```

Se usará la imagen oficial de Docker de Ubuntu como se especifica en la línea `FROM ubuntu`.

Cuando se ejecuta `apt-get update -y` o `apt update -y`, te pedirá configurar la zona horaria, el prompt esperará para que escribas la opción seleccionada.

Para evitar esto, tienes que agregar las opciones de configuración en el `Dockerfile`, como se describe [aquí](https://dev.to/setevoy/docker-configure-tzdata-and-timezone-during-build-20bk), agregando las siguientes líneas:

```docker
ENV TZ=America/Mexico_City
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
```

Reemplaza el valor de `TZ` de acuerdo a tu zona horaria.

Vagrant requiere una conexión SSH para acceder al contenedor y las imágenes de Docker vienen con el usuario `root`. Tienes que configurar otro usuario con permisos de `root`. Es por eso que se necesitan los paquetes `ssh` y `sudo`.

En las siguientes líneas se crea el usuario `vagrant` y se le asigna una contraseña. No se solicitará al usuario que proporcione alguna contraseña cuando ejecute cualquier comando que requiera permisos de `root`. El usuario también se agrega al grupo `sudo`.

```docker
RUN useradd --create-home -s /bin/bash vagrant
RUN echo -n 'vagrant:vagrant' | chpasswd
RUN echo 'vagrant ALL = NOPASSWD: ALL' > /etc/sudoers.d/vagrant
```

Se debe crear el directorio `.ssh`. Este es el directorio donde se almacenan los archivos de configuración relacionados con la conexión SSH.

```docker
RUN mkdir -p /home/vagrant/.ssh
RUN chmod 700 /home/vagrant/.ssh
```

Se agrega una llave insegura para la configuración inicial. Esta llave se reemplazará después cuando inicialices el entorno virtual por primera vez. Además, la propiedad del directorio `.ssh` se asigna al usuario `vagrant`.

```docker
RUN echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ==" > /home/vagrant/.ssh/authorized_keys
RUN chmod 600 /home/vagrant/.ssh/authorized_keys
RUN chown -R vagrant:vagrant /home/vagrant/.ssh
```

Puedes iniciar sesión con el usuario `root`, pero no se asignó una contraseña. Puedes cambiar la contraseña agregando una línea similar, pero cambiando `vagrant:vagrant` por `root:THEPASSWORDYOUCHOOSE` o después de iniciar sesión.

### Vagrantfile
Ahora crea un `Vagrantfile`:

```bash
$ touch Vagrantfile
```

Y agrega el siguiente contenido:

```ruby
Vagrant.configure("2") do |config|
  config.vm.provider :docker do |d|
     d.build_dir = "."
     d.remains_running = true
     d.has_ssh = true
  end
end
```

Aquí le indicas a Vagrant que debe construir la imagen de Docker a partir del `Dockerfile` y se puede acceder al contenedor a través de SSH. El contenedor debe estar siempre ejecutándose.

```ruby
d.build_dir = "."
d.remains_running = true
d.has_ssh = true
```

Para instalar software puedes usar un script de Bash o cualquier herramienta de aprovisionamiento soportada por Vagrant. También puedes agregar al `Dockerfile` las instrucciones para instalar y configurar las herramientas que necesitas.

Si quieres usar un script de Bash, solo agrega la siguiente línea después de `config.vm.provider`.

```ruby
config.vm.provision :shell, path: "script.sh", privileged: false
```

La opción `privileged` se configura como `false` ya que no necesitarás ejecutar estos comandos con permisos de `root`.

### Inicializa y Ejecuta
Cuando ejecutes `vagrant up`, Vagrant construirá la imagen de Docker a partir del `Dockerfile` y ejecutará el contenedor.

Puedes iniciar sesión en el entorno virtual ejecutando `vagrant ssh`.

Si quieres detener el entorno virtual, ejecuta `vagrant halt`. Para destruir el entorno ejecuta `vagrant destroy`.