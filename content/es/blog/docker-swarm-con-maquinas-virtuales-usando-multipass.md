---
title: "Docker Swarm con Máquinas Virtuales usando Multipass"
date: 2023-12-31
description: "Cómo configurar Docker Swarm en un entorno de desarrollo usando máquinas virtuales"
summary: "Aprende a configurar un swarm de tres nodos usando máquinas virtuales de Ubuntu"
tags: ["tutorial", "docker", "virtualization"]
---

De acuerdo a la documentación de Docker, El modo [swarm](https://docs.docker.com/engine/swarm/key-concepts/) es una funcionalidad avanzada para gestionar un clúster de procesos de Docker. Cada instancia de Docker Engine participando en el swarm es un nodo, y actuan como administradores (managers), para administrar a los miembros del swarm y delegación de tareas, y trabajadores (workers),que se encargan de ejecutar los servicios del swarm. Un determinado host de Docker puede ser un administrador, un trabajador, o realizar ambos roles. Puedes ejecutar uno o más nodos en una sola computadora física o servidor en la nube, pero en producción, un swarm generalmente incluye nodos de Docker distribuidos en múltiples máquinas físicas o en la nube.

En un entorno de desarrollo, donde probablemente no se tenga acceso a múltiples dispositivos físicos para configurar un swarm, una solución sería create máquinas virtuales, cada una de ellas ejecutando una instancia de Docker Engine.

El mes pasado, escribí un tutorial sobre [Multipass](https://mariog.dev/blog/multipass-maquinas-virtuales-de-ubuntu-de-forma-facil/), una herramienta de Canonical que puede usarse para crear máquinas virtuales de Ubuntu, y a través de este artículo, aprenderás como usarla para configurar el swarm. Seguí las instrucciones de [este artículo](https://techsparx.com/software-development/docker/swarm/multipass.html), pero explicaré el proceso general y como desplegar servicios en el swarm.

### Máquinas Virtuales
Para crear máquinas virtuales con, ejecutarías el siguiente comando:

```
$ multipass launch --name node1 lunar
```

El comando anterior crearía una máquina virtual, llamada `node1`, con las siguientes características:

* 1 CPU
* 5GB de disco
* 1GB de RAM

Dependiendo de los requerimientos de tu entorno de desarrollo, la configuración por defecto puede ser limitada, y probablemente necesites asignar más recursos. Sugiero crear una máquina virtual con las siguientes características si se necesitan más recursos:

* 2 CPUs
* 8GB de disco
* 2GB de RAM

Estoy realizando pruebas en una computadora con 4 CPUs y 12GB de RAM, y asignar los recursos anteriores a cada nodo, no afecta la operación normal del host.

Es momento de crear un clúster de tres nodos.

Primero, crea un script de BASH (`init-instance.sh`):

```
NM=$1

multipass launch --name ${NM} lunar --memory 2G --disk 8G --cpus 2

multipass transfer install-docker.sh ${NM}:/home/ubuntu/install-docker.sh
multipass exec ${NM} -- sh -x /home/ubuntu/install-docker.sh
```

1. `NM` es la variable que obtendrá el nombre de la máquina virtual desde la línea de comandos
2. Creará una máquina virtual personalizada
3. El archivo `install-docker.sh` se transferirá del host a la máquina virtual
4. El script instalará Docker Engine en cada nodo

Ahora crea el script para instalar Docker Engine en cada nodo del swarm.

```
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install -y ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/u>  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

# Install latest version
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Manage Docker as a non-root user
## Create the docker group
sudo groupadd docker

## Add your user to the docker group
sudo usermod -aG docker $USER

## Enable the docker daemon to run on system boot
sudo systemctl enable docker
```

El script:

* Agregará la llave GPG oficial de Docker
* Agregará el repositorio a los recursos de Apt
* Instalará la última versión de Docker Engine
* Se gestionará Docker como un usuario sin acceso de superusuario
    * Creará el grupo `docker`
    * Agregará el usuario (`ubuntu`) de la máquina virtual al grupo `docker`

Docker Swarm usa el controlador `overlay` por defecto, y no es soportado por el modo rootless. Es por eso que Docker debe configurarse siguiendo las instrucciones [post-installation](https://docs.docker.com/engine/install/linux-postinstall/).

Ahora ejecuta el siguiente comando para crear el nodo administrador (`manager`):

```
$ sh -x init-instance.sh manager
```

En seguida, crea dos nodos trabajadores, `worker1` y `worker2`:

```
$ sh -x init-instance.sh worker1
```

```
$ sh -x init-instance.sh worker2
```

Las instancias de Docker Engine están ejecutándose.

## Inicializa el Swarm
El swarm debe inicializarseen el nodo administrador (`manager`), y los máquinas virtuales `worker1` y `worker2` deben agregarse al swarm como nodos trabajadores.

Inicializa el swarm:

```
$ multipass exec manager -- docker swarm init
```

Obtendrás la siguiente salida:

```
Swarm initialized: current node (fgjgd7qr4yg0dnlk8nurrqk9q) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-5d9bj8wua4wy812hzl7vqc2r54yroy227i2oqkl27k3vzxdk7e-7na4j9souwf0s2yf6tmw5daiq 10.81.157.203:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

Devolverá el token y el comando necesario para agregar los nodos trabajadores al swarm. `10.81.157.203` es la dirección IP asignada por Multipass a la máquina virtual `manager`.

Agrega la máquina virtual `worker1` al swarm como trabajador:

```
$ multipass exec worker1 -- docker swarm join --token SWMTKN-1-5d9bj8wua4wy812hzl7vqc2r54yroy227i2oqkl27k3vzxdk7e-7na4j9souwf0s2yf6tmw5daiq 10.81.157.203:2377
```

Agrega la máquina virtual `worker2` al swarm como trabajador:

```
$ multipass exec worker2 -- docker swarm join --token SWMTKN-1-5d9bj8wua4wy812hzl7vqc2r54yroy227i2oqkl27k3vzxdk7e-7na4j9souwf0s2yf6tmw5daiq 10.81.157.203:2377
```

El swarm ya está configurado. Puedes listar los nodos en el swarm ejecutando:

```
$ multipass exec manager -- docker node ls
```

## Despliega un Servicio al Swarm
Ahora que el swarm está configurado, puedes ejecutar contenedores independientes en cada nodo, pero no los gestionará el nodo administrador, solo los servicios desplegados al swarm.

Es momento de desplegar el primer servicio, un servidor de Nginx con tres réplicas.

```
$ multipass exec manager -- docker service create --name nginx --replicas 3 -p 80:80 nginx
```

* El comando `docker service create` crea el servicio
* La opción `--name` se usa para asignar un nombre al servicio, `nginx`
* La opción `--replicas` especifica el número de réplicas
* La opción `-p` se usa para abrir el puerto `80` en el host para recibir y redirigir todas las peticiones al puerto `80` del contenedor

Puedes revisar los servicios que se están ejecutando con el siguiente comando:

```
$ multipass exec manager -- docker service ls
```

Obtendrás la siguiente salida:

```
ID             NAME      MODE         REPLICAS   IMAGE          PORTS
5rflc7yq4pyg   nginx     replicated   3/3        nginx:latest   *:80->80/tcp
```

Para ver que nodos están ejecutando el servicio, ejecuta el siguiente comando:

```
$ multipass exec manager -- docker service ps nginx
```

Obtendrás la siguiente salida:

```
ID             NAME      IMAGE          NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
ysp4p3ut581a   nginx.1   nginx:latest   worker1   Running         Running 6 minutes ago
7zozyfe4s8a5   nginx.2   nginx:latest   worker2   Running         Running 6 minutes ago
njdou1bmh4tm   nginx.3   nginx:latest   manager   Running         Running 6 minutes ago
```

Para mostrar los detalles de un servicio en un formato que sea fácil de leer, ejecuta:

```
$ multipass exec manager -- docker service inspect --pretty nginx
```

Mostrará:

```
ID:             5rflc7yq4pygo7kxu8pq7ev7l
Name:           nginx
Service Mode:   Replicated
 Replicas:      3
Placement:
UpdateConfig:
 Parallelism:   1
 On failure:    pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:   1
 On failure:    pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:         nginx:latest@sha256:2bdc49f2f8ae8d8dc50ed00f2ee56d00385c6f8bc8a8b320d0a294d9e3b49026
 Init:          false
Resources:
Endpoint Mode:  vip
Ports:
 PublishedPort = 80
  Protocol = tcp
  TargetPort = 80
  PublishMode = ingress
```

Si quieres eliminar el servicio, escribe:

```
$ multipass exec manager -- docker service rm nginx
```

## Conclusión
A través de este artículo, aprendiste como configurar un swarm usando máquinas virtuales, y desplegaste tu primer servicio.