---
title: "MongoDB: Cómo Convertir Documentos BSON a JSON"
date: "2023-09-29"
description: "Cómo Convertir Documentos BSON a JSON"
summary: "Aprende como puedes convertir documentos BSON a un formato que sea fácil de leer"
tags: ["mongodb", "python", "bash"]
---

Binary Javascript Object Notation (BSON) es una serialización codificada en binario de documentos JSON. JSON es más fácil de entender ya que es legible por humanos, pero comparado con BSON, soporta menos tipos de datos. BSON se ha extendido para agregar algunos tipos de datos opcionales no nativos de JSON, como fechas y datos binarios.

MongoDB almacena información en formato BSON tanto internamente como a través de la red. Es también el formato usado para los archivos generados por mongodump. Para leer el contenido de un documento BSON, tienes que convertirlo a un formato legible por humanos, como JSON.

A través de este artículo, aprenderás como convertir un documento BSON a JSON. Algunos de los métodos que explicaré incluyen el uso de bsondump, mongoexport, Python y Bash.

## BSON a JSON con bsondump

[bsondump](https://www.mongodb.com/docs/database-tools/bsondump/) convierte archivos [BSON](https://www.mongodb.com/docs/manual/reference/glossary/#std-term-BSON) a formatos legibles por humanos, incluyendo [JSON](https://www.mongodb.com/docs/manual/reference/glossary/#std-term-JSON). Por ejemplo, bsondump es útil para leer los archivos generados por [mongodump](https://www.mongodb.com/docs/database-tools/mongodump/#mongodb-binary-bin.mongodump). bsondump es una herramienta que forma parte del paquete [MongoDB Database Tools](https://www.mongodb.com/docs/database-tools/installation/installation/).

Ejecuta `bsondump` desde la línea de comandos del sistema:

```bash
bsondump --outFile=collection.json collection.bson
```

Creará un archivo JSON (`collection.json`) a partir de un documento BSON existente (`collection.bson`), como los que se crean después de respaldar la base de datos.

## BSON a JSON con mongoexport

[mongoexport](https://www.mongodb.com/docs/database-tools/mongoexport) es una herramienta de línea de comandos que produce un JSON o CSV a partir de información almacenada en una instancia de MongoDB. LA herramienta mongoexport es parte del paquete [MongoDB Database Tools](https://www.mongodb.com/docs/database-tools/installation/installation/).

Ejecuta `mongoexport` desde la línea de comandos:

```bash
mongoexport --collection=employees --db=company --out=employees.json --pretty
```

Para conectarse a una instancia local de MongoDB que usa el puerto 27017, no es necesario especificar el host o el puerto. En caso contrario, revisa la sección [Connect to a MongoDB Instance](https://www.mongodb.com/docs/database-tools/mongoexport/#connect-to-a-mongodb-instance) en la documentación para mayor información.

La opción `--pretty` le dará formato al contenido del archivo JSON.

## BSON a JSON con Python
Si desarrollas con Python, hay dos formas de leer un documento BSON y convertirlo a JSON.

1. Usando el módulo `bson` de la biblioteca [PyMongo](https://pymongo.readthedocs.io/en/stable/)

```python
from bson import decode_all
from bson.json_util import dumps

with open('./data.bson','rb') as f:
    data = decode_all(f.read())

with open("./data.json", "w") as outfile:
    outfile.write(dumps(data, indent=2))
```

Esto es lo que el script está haciendo:
<ol type="a">
  <li>Importa los métodos `decode_all` y `dumps` del módulo `bson`</li>
  <li>Abre el archivo para leer el contenido y decodificar los datos</li>
  <li>Crea un archivo JSON, y escribe los datos obtenidos del archivo BSON en el archivo JSON</li>
</ol>

El script funciona con archivos BSON generado por mongodump. Antes de ejecutar el script, debes instalar PyMongo: `pip install pymongo`.

2. Conectarse a la base de datos y consultar los datos con PyMongo, el driver de Python para MongoDB.

```python
from pymongo import MongoClient
from bson.json_util import dumps

uri = "mongodb://username:password@host:port/"
client = MongoClient(uri)

db = client.company
employees = db.employees

cursor = employees.find()
list_cur = list(cursor)

json_data = dumps(list_cur, indent = 2)

with open('data.json', 'w') as file:
    file.write(json_data)
```

Esto es lo que hace el script:

<ol type="a">
  <li>Importa el método `MongoClient` de la biblioteca `pymongo`, y el método `dumps` del módulo `bson`</li>
  <li>Establece la conexión a la base de datos</li>
  <li>Especifica la base de datos (e.g., `company`) y la colección (e.g., `employees`) que se quiere consultar</li>
  <li>Obtiene los documentos en la colección con el método `find()` y crea una lista con el resultado. Si no pasas ningún parámetro a este método, el resultado será similar a `SELECT *` como en MySQL</li>
  <li>Crea un objeto JSON llamando al método `dumps`. El parámetro `indent = 2` le dirá al método `dumps()` que le de formato al objeto JSON</li>
  <li>Escribe el contenido de la variable `json_data` en el archivo `data.json`</li>
</ol>

Antes de ejecutar el script, debes instalar PyMongo: `pip install pymongo`.

## BSON a JSON con Bash
Le pregunté a la IA de [phind.com](phind.com) que me mostrara cómo convertir un archivo BSON a JSON, y una de las soluciones que mostró fue crear un script de Bash en el directorio donde se encuentran los archivos BSON.

```bash
#!/bin/bash
declare -a bson_files
bson_files=( $(ls -d $PWD/*.bson) )

for file in "${bson_files[@]}"; 
do 
bsondump $file --outFile=$file.json
done
```

Este script lista todos los archivos BSON en el directorio actual y guarda el resultado en un arreglo, luego recorre el arreglo y convierte cada archivo BSON a archivos JSON. El script usa `bsondump`.

Para ejecutar el script

1. Agrega permiso de ejecución al script: `chmod +x bson_to_json.sh`
2. Ejecuta este comando en la línea de comandos:

```bash
./bson_to_json.sh
```

## Conclusión
Si quieres leer el contenido de un documento BSON, puedes usar bsondump y mongoexport para convertir un documento BSON a un formato legible por humanos como JSON. Estas herramientas son parte del paquete [MongoDB Database Tools](https://www.mongodb.com/docs/database-tools/installation/installation/).

Si desarrollas, puedes usar el driver de MongoDB de tu lenguaje de programación preferido, y consultar los datos para analizar el contenido de las colecciones en tu base de datos. Para Python, puedes instalar PyMongo, establecer la conexión a la base de datos, consultar los datos, y usar el módulo bson para almacenar el contenido en un documento JSON.

Hay otras soluciones, como herramientas en línea, y métodos proporcionados por otros lenguajes de programación, pero aquí aprendiste algunas formas de hacerlo.

---

Artículo original publicado en inglés en [percona.com](https://www.percona.com/blog/mongodb-how-to-convert-bson-documents-to-a-human-readable-format/)
