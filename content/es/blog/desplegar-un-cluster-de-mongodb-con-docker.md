---
title: "Desplegar un Clúster de MongoDB con Docker"
date: 2023-12-27
description: "Cómo desplegar un cluster de MongoDB usando Docker"
summary: "Aprende a desplegar un cluster de MongoDB con replicación habilitada"
tags: ["tutorial", "docker", "linux", "mongodb"]
series: ["Percona Backup for MongoDB en un Entorno de Desarrollo con Docker"]
series_order: 1
---

[Percona Backup for MongoDB](https://docs.percona.com/percona-backup-mongodb/index.html) (PBM) es una solución de código abierto, distribuida y de bajo impacto, para respaldos consistentes de clústeres fragmentados de MongoDB y conjuntos de réplicas.

PBM no funciona en instancias independientes de MongoDB, ya que requiere un [oplog](https://docs.percona.com/percona-backup-mongodb/reference/glossary.html#oplog) (registro de operaciones) para garantizar la consistencia de los respaldos. Oplog solo está disponible en nodos con replicación habilitada.

Puedes desplegar Percona Backup for MongoDB para fines de prueba en un conjunto de réplica de un solo nodo. Sigue [esta guía](https://docs.mongodb.com/manual/tutorial/convert-standalone-to-replica-set/) de la documentación de MongoDB para aprender como convertir una instancia independiente en un conjunto de réplica.

También puedes usar Docker para desplegar un clúster de MongoDB en tu computadora personal, así como configurar el almacenamiento y ejecutar PBM. Usar Docker puede ser útil para probar PBM en un entorno de desarrollo antes de ejecutarlo en producción.

A través de esta serie de artículos, aprenderás a configurar un entorno de desarrollo para PBM, empezando con la creación de un conjunto de réplica de tres nodos, un nodo primario y dos secundarios. Continuando con la configuración de MinIO para almacenar los respaldos de tus bases de datos, y finalmente configurar PBM y realizar pruebas.

Esta serie incluye los siguientes artículos:

1. Desplegar un Clúster de MongoDB con Docker
2. Almacenamiento de Objectos con MinIO en Docker
3. Ejecutar Percona Backup for MongoDB con Docker

Para desplegar un clúster de MongoDB, he seguido las instrucciones de [este tutorial](https://www.mongodb.com/compatibility/deploying-a-mongodb-cluster-with-docker), pero explicaré el proceso general para obtener un clúster con replicación habilitada y listo para usarse.

## Desplegar un Clúster de MongoDB
![MongoDB Cluster](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/mongodb-cluster.jpg)

Puedes desplegar clústeres de MongoDB de diferentes maneras:
* Instalar MongoDB en tu computadora y [desplegar un conjunto de réplica](https://docs.mongodb.com/manual/replication/)
* Usar Docker para ejecutar el clúster
* Desplegar un clúster altamente disponible en Kubernetes

Para el propósito de esta serie. se usará Docker ya que es fácil de usar y una forma de probar métodos de respaldo sin necesitar hardware adicional. Estos son los pasos para crear un clúster de MongoDB con Docker:

1. Crear una red de Docker
2. Iniciar tres instancias de MongoDB
3. Inicializar el conjunto de réplica

### Crear una Red de Docker
Primero, crea una red de Docker, ejecutando el siguiente comando:

```
$ docker network create mongodbCluster
```

Si la red se ha creado, puedes obtener su información mediante el siguiente comando:

```
$ docker network inspect mongodbCluster
```

La salida del comando anterior se mostrará así:

```
[
    {
        "Name": "mongodbCluster",
        "Id": "ebde001d9f02012f803a5914a3b450f6e6092a972ccfd513afb61399cbdca2a4",
        "Created": "2023-02-23T16:02:13.629671156Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.19.0.0/16",
                    "Gateway": "172.19.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]

```

### Inicializar las Instancias de MongoDB
Crea tu nodo primario y agrégalo a la red que se creó anteriormente:

```
$ docker run -d -p 27017:27017 --name mongo1 --network mongodbCluster mongo:6 mongod --replSet myReplicaSet --bind_ip localhost,mongo1
```

Los parámetros en el comando anterior corresponden a:
* `-d`. Ejecutar el contenedor en segundo plano
* `-p`. El puerto en la máquina host que recibirá y redirigirá todas las peticiones al puerto del contenedor
* `--name`. Nombre del contenedor
* `--network`. Nombre de la red a usar
* `mongo:6`. Nombre de la imagen base usada para crear el contenedor

El siguiente comando será ejecutado en el contenedor iniciado:

```
mongod --replSet myReplicaSet --bind_ip localhost,mongo1
```

Donde:

* La opción `--replSet` se usa para asignar un nombre (`myReplicaSet`) al conjunto de réplica
* La opción `--bind_ip` es usada para asegurar que MongoDB escuche las conexiones de aplicaciones en las direcciones configuradas. En este caso, la instancia de `mongod` se asocia a `localhost` y el nombre del host (`mongo1`)

Una vez que el nodo primario es creado, debes crear los nodos secundarios del conjunto de réplica:

```
$ docker run -d -p 27018:27017 --name mongo2 --network mongodbCluster mongo:6 mongod --replSet myReplicaSet --bind_ip localhost,mongo2
```

```
$ docker run -d -p 27019:27017 --name mongo3 --network mongodbCluster mongo:6 mongod --replSet myReplicaSet --bind_ip localhost,mongo3
```

### Inicializar el Conjunto de Réplica
Después de que el clúster es creado, debes inicializar el conjunto de réplica. Ejecuta el método `rs.initiate()` en el nodo primario de tu clúster:

```
$ docker exec -it mongo1 mongosh --eval "rs.initiate({
 _id: \"myReplicaSet\",
 members: [
   {_id: 0, host: \"mongo1\"},
   {_id: 1, host: \"mongo2\"},
   {_id: 2, host: \"mongo3\"}
 ]
})"
```

Si el conjunto de réplica fue inicializado, obtendrás la siguiente salida:

```
{ ok: 1 }
```

Revisa el estado del conjunto de réplica con el siguiente comando:

```
docker exec -it mongo1 mongosh --eval "rs.status()"
```

### Autenticación
Para autenticarse en el clúster de MongoDB, crea un usuario con los roles [userAdminAnyDatabase](https://www.mongodb.com/docs/manual/reference/built-in-roles/#mongodb-authrole-userAdminAnyDatabase) y [readWriteAnyDatabase](https://www.mongodb.com/docs/manual/reference/built-in-roles/#mongodb-authrole-readWriteAnyDatabase), siguiendo las instrucciones [acá](https://www.mongodb.com/docs/manual/tutorial/configure-scram-client-authentication/). 

Inicia sesión en tu servidor de MongoDB:

```
$ docker exec -it mongo1 mongosh
```

Cambiar a la base de datos `admin`

```
$ use admin
```

Y agrega un nuevo usuario:

```
db.createUser(
 {
   user: "myUserAdmin",
   pwd: passwordPrompt(), // or cleartext password
   roles: [
     { role: "userAdminAnyDatabase", db: "admin" },
     { role: "readWriteAnyDatabase", db: "admin" }
   ]
 }
)
```

Reemplaza `myUserAdmin` con el nomnbre de usuario deseado. Al llamar a `passwordPrompt()`, se te pedirá que escribas la contraseña que quieres asignar, o puedes escribir el valor ahí en lugar de llamar a ese método.

Una vez que el usuario es creado, puedes iniciar sesión desde la línea de comandos:

```
$ mongosh --username myUserAdmin --password --host mongo1
```

Se te solicitará que escribas la contraseña. Si estás accediendo desde fuera del clúster, debes reemplazr `mongo1` con la dirección IPdel nodo primario de tu clúster.

O puedes pasar una cadena de conexión:

```
$ mongosh “mongodb://myUserAdmin:password@localhost/mydb”
```

Reemplaza `password`, `localhost`, y `mydb` con tu información de autenticación.

## Conclusión
A través de este artículo, aprendiste a configurar un clúster de MongoDB con replicación habilitada usando Docker. Esta es la primera parte de la serie de artículos que te guiará para configurar Percona Backup for MongoDB en un entorno de desarrollo antes de usarlo en producción.