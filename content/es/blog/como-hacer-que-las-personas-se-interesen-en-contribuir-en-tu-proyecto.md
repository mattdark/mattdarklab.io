---
title: "Cómo Hacer Que Las Personas Se Interesen en Contribuir en Tu Proyecto"
date: 2024-02-05
description: "Comparto algunas recomendaciones de puedes lograr que otras personas se interesen por tu proyecto"
summary: "En este texto aprenderás como lograr que las personas se interesen en contribuir en tu proyecto"
tags: ["community"]
---

Así que tienes un proyecto que decidiste publicar bajo una licencia abierta, ¿qué sigue? ¿Cómo logras que las personas se interesen en contribuir en tu proyecto abierto?

Primero, cuando digo proyecto abierto, me refiero a cualquier proyecto publicado bajo una licencia como [GPL](https://www.gnu.org/licenses/licenses.html), cualquier licencia de Código Abierto [license](https://opensource.org/licenses/), o una licencia [Creative Commons](https://creativecommons.org). No todos los proyectos están relacionados con desarrollo de software. Hay proyectos sobre creación de contenido multimedia, como imágenes, texto, audio o video, y si quieres que cualquiera tenga acceso, pueda redistribuir el material o crear trabajos derivados, como sucede con el Software Libre y de Código Abierto, tendrás que usar una licencia Creative Commons, o GFDL si es un proyecto de documentación.

En mi artículo anterior, [Una Guía Para Contribuir Al Código Abierto Sin Tener Conocimientos Técnicos](https://mariog.dev/blog/una-guia-para-contribuir-al-codigo-abierto-sin-tener-conocimientos-tecnicos/), escribí sobre como contribuir al Código Abierto en áreas que no están relacionadas con desarrollo de software, y es algo que debe considerarse cuando se buscan personas que se unan al proyecto como contribuidores. Puedes necesitar un logo para tu proyecto, alguien que escriba la documentación o la traduzca a otro idioma.

A través de este artículo, aprenderás como preparar tu proyecto e invitar a otras personas a contribuir.

## Primeros Pasos
Aquí hay algunos pasos a seguir para preparar tu proyecto:

* Crea un repositorio para tu proyecto
    * Sigue [esta guía](https://www.atlassian.com/git/tutorials/setting-up-a-repository) de Atlassian

* Documenta tu proyecto y los procesos relacionados
    * Crea un `README`
        De acuerdo a Hillary Nyakundi, en [este artículo](https://www.freecodecamp.org/news/how-to-write-a-good-readme-file/), publicado en [freeCodeCamp](https://www.freecodecamp.org), un `README` es un archivo que le proporciona al usuario una descripción detallada del proyecto en el que has estado trabajando.

        También puede describirse como documentación con guías de como usar el proyecto. Generalmente tendrá instrucciones de como instalar e iniciar el proyecto.

        Puedes usar una plantilla para crear el `README` para tu proyecto:
            * [GitHub README Templates](https://www.readme-templates.com/)
            * [A template to make good README.md](https://github.com/PurpleBooth/a-good-readme-template)

        Usar el editor de [readme.so](https://readme.so)

        O seguir las recomendaciones en [Make a README](https://www.makeareadme.com)

    * Agrega el archivo `CONTRIBUTING.md`
        Este archivo contiene instrucciones de como las personas pueden contribuir a tu proyecto, sin importar si es en el código, la documentación, traducciones u otro tipo de contribuciones.

        Algunos recursos útiles:
            * [How to Build a CONTRIBUTING.md](https://mozillascience.github.io/working-open-workshop/contributing/)
            * [Setting guidelines for repository contributors](https://docs.github.com/en/communities/setting-up-your-project-for-healthy-contributions/setting-guidelines-for-repository-contributors)

        ¿Necesitas un ejemplo? Revisa la [guía de contribución](https://github.com/github/docs/blob/09be36847cdeaa64867b61c8d0005db005b61843/.github/CONTRIBUTING.md) a la documentación de GitHub.
        
    * Agrega el archivo `LICENSE`
        Dependiendo de las características de tu proyecto, como mencioné anteriormente, puedes usar una licencia como [GPL](https://www.gnu.org/licenses/licenses.html) o cualquier [license]([license](https://opensource.org/licenses/)) de Código Abierto aprovada por la Open Source Initiative, o una licencia [Creative Commons](https://creativecommons.org) para contenido multimedia, o GFDL si es un proyecto de documentación.

        [Choose a License](https://choosealicense.com/) te puede ayudar a elegir la licencia correcta para tu proyecto.

    * Agrega el archivo `CODE_OF_CONDUCT.md`
        De acuerdo a la [documentación de GitHub](https://docs.github.com/en/communities/setting-up-your-project-for-healthy-contributions/adding-a-code-of-conduct-to-your-project), un código de conducta define estándares sobre cómo participar en una comunidad. Señala un entorno inclusivo que respeta todas las contribuciones. También describe los procedimientos para abordar problemas entre miembros de la comunidad.

        Muchos proyectos de código abierto han adoptado el [Contributor Covenant](https://www.contributor-covenant.org/) cómo su código de conducta. Revisa [esta página](https://www.contributor-covenant.org/adopters/) para ver una lista de quienes lo están usando.

    * Crea la documentación técnica de tu proyecto
        Puedes usar cualquiera de las siguientes opciones:
            * Una wiki, cómo [ArchWiki](https://wiki.archlinux.org) que usa [MediaWiki](https://www.mediawiki.org/wiki/MediaWiki)
            * [Read the Docs](https://about.readthedocs.com/), usado por proyectos cómo [Setuptools](https://setuptools.pypa.io/en/latest/). Revisa [Awesome Read the Docs](https://github.com/readthedocs-examples/awesome-read-the-docs) para ver más ejemplos.
            * Crea un sitio web
            * Crea un blog, cómo la documentación de [Blowfish](https://blowfish.page/), un tema para [Hugo](https://gohugo.io/). 

* Configura los canales de comunicación
    Hay muchas opciones para configurar los canales de comunicación de tu proyecto, pero usa el menor número de opciones posibles. Para alguien sin un perfil técnico, puede ser difícil aprender a usar tantas herramientas cómo probablemente decidas usar. Mantén la comunicación tan simple cómo sea posible.

    Acá hay algunas opciones:
        * Listas de correos
        * Gitter, Slack, IRC, Telegram
        * Redes sociales

## Crea Issues
De la documentación de GitLab, usa [issues](https://docs.gitlab.com/ee/user/project/issues/) para colaborar en ideas, resolver problemas, y planear el trabajo. Comparte y discute propuestas con tu equipo y con colaboradores externos.

¿Cómo agregar los primeros issues a tu proyecto? Primero, haz una lista de las tareas que deben realizarse y para ello puedes usar:

* Un pizarrón o una ventana
* Un cuaderno o notas adhesivas
* Aplicaciones para tomar notas. Acá hay una lista de [aplicaciones para tomar notas en Linux]((https://itsfoss.com/note-taking-apps-linux/))

Luego, agrega los issues en el repositorio de tu proyecto. Revisa las instrucciones para:

* [GitHub](https://docs.github.com/en/issues/tracking-your-work-with-issues/creating-an-issue)
* [GitLab](https://docs.gitlab.com/ee/user/project/issues/create_issues.html)

No olvides etiquetar tus issues apropiadamente. Para personas sin experiencia previa contribuyendo a proyectos de Código Abierto o cualquier proyecto abierto, debes considerar incluir issues con las siguientes etiquetas:

* `good first issue`
* `help wanted`
* `first timers only`

Más información sobre etiquetas:

* [GitHub](https://docs.github.com/en/issues/using-labels-and-milestones-to-track-work/managing-labels)
* [GitLab](https://docs.gitlab.com/ee/user/project/labels.html)

Los issues pueden ser técnicos o no técnicos, y estar relacionados a:
* Reporte de errores
* Solicitud de funcionalidades
* Documentación
* Localización
* UI/UX
* Diseño

### ¿Qué Hay en un Issue?
Un buen issue tiene:

* Instrucciones de cómo empezar
* Requisitos claros
* Referencias a información relevante, incluyendo:
    * Guías de contribución
    * Código de conducta
    * Licencia

Tus usuarios y contribuidores también pueden crear issues para reportar errores o solicitar nuevas funcionalidades. Acá encontrarás más información sobre cómo agregar plantillas para tus issues, y pull requests o merge requests:

* [GitLab - Description templates](https://docs.gitlab.com/ee/user/project/description_templates.html)
* [GitHub - Configuring issue templates for your repository](https://docs.github.com/en/communities/using-templates-to-encourage-useful-issues-and-pull-requests/configuring-issue-templates-for-your-repository)

Cuando un nuevo contribuidor llega:
* Responde con un mensaje de bienvenida como "¡Gracias, estoy para ayudar si tienes alguna pregunta!", o agrega el [welcome-bot](https://github.com/behaviorbot/welcome)
* Comprométete a responder a las consultas

### ¿Qué es un pull/merge request?
Un [pull request](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/proposing-changes-to-your-work-with-pull-requests/about-pull-requests), cómo lo define GitHub, es una propuesta para mezclar un conjunto de cambios de una rama del repositorio a otra. En un pull request, colaboradores pueden revisar y discutir los cambios propuestos antes de que los cambios se integren a la base de código principal. Pull requests muestran las diferencias, o diffs, entre el contenido en la rama origen y el contenido en la rama objetivo.

GitLab define un [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) cómo una propuesta que incorpora cambios de una rama origen a una rama objetivo. Los merge requests incluyen:

* Una descripción de la solicitud.
* Cambios en el código y revisiones.
* Información sobre pipelines de CI/CD.
* Una sección de comentarios para hilos de discusión.
* La lista de commits. 

## Asesorar Contribuidoeres
Issues con la etiqueta `first timers only` son buenos para incorporar nuevos contribuidores mediante mentoría. Muchas personas empiezan a contribuir al Código Abierto para aprender nuevas tecnologías o mejorar sus habilidades. Si tienes tiempo para guiar contribuidores, agrega tu nombre como mentor en el issue.

¿Debería continuar asesorando a este contribuidor? Acá hay algunas pautas generales al momento de elegir en que contribuidores invertir tiempo:

* **Disponible**. Tiene el tiempo y quiere contribuir y aprender
* **Dispuesto a aprender**. Puede aprender de ti y la comunidad
* **Apasionado**. Tiene un interés genuino por el proyecto y la misión
* **Habilidades**. ¿Tienen las habilidades que necesitas para tu proyecto?

### Soporte Continuo

* Prestar atención a los mensajes enviados a los canales de comunicación configurados para tu proyecto, y responder cualquier pregunta que pueda surgir
* Ayuda a quienes no conocen Git mientras resuelven su primer issue
    Acá hay algunos recursos útiles que puedes compartir:
        * [Getting started with Git](https://docs.github.com/en/get-started/getting-started-with-git)
        * [Learn Git](https://docs.gitlab.com/ee/topics/git/)
* Dirige a las personas a issues con los que puedan empezar y realiza el seguimiento adecuado

## Después de Contribuir
Una vez que han contribuido/enviado un pull/merge request:
* Agradeceles por su trabajo. Puedes usar el [welcome-bot](https://github.com/behaviorbot/welcome)
* Proporciona comentarios buenos, consistentes y útiles durante una revisión - haz preguntas sobre lo que hicieron

Después de que su trabajo es mezclado:
* Aprecia su trabajo. Puedes agregar la sección [all-contributors](https://github.com/kentcdodds/all-contributors) al README de tu proyecto
* Dirígeles al siguiente issue o tarea en el que puedan trabajar

¿Tienes swag para obsequiar? Stickers, camisetas, etc. Esa es otra forma de agradecer a tus contribuidores por su trabajo y el tiempo que dedican al proyecto.

## Atraer Contribuidores
¿Quieres promover tu proyecto de Código Abierto? Dependiendo de tu disponiblidad, acá hay algunas opciones a considerar que te pueden ayudar a atraer contribuidores a tu proyecto:

* Crea una lista de correos para mantener a tus usuarios y contribuidores actualizados sobre tu proyecto
* Si ya te registraste a una lista de correos, envía un correo con información sobre tu proyecto y oportunidades de contribución
* Publica información sobre tu proyecto en redes sociales
* Usa aplicaciones de mensajería para invitar a personas
* Escribe un artículo sobre tu proyecto
* Crea un video y publicalo en redes sociales
* Asiste u organiza eventos
    * Organiza un taller
    * Organiza un hackathon
    * Únete a [Hacktoberfest](https://hacktoberfest.com/)
    * Participa cómo conferencista en una conferencia o meetup

### La Regla Pac-Man
Asistir a eventos puede ser una oportunidad para promover tu proyecto, ya que puedes encontrar personas a las que les interesaría contribuir en tu proyecto. 

Mientras asistes a un eventos y te unes a una conversación, no olvides seguir la[regla Pac-Man](https://www.ericholscher.com/blog/2017/aug/2/pacman-rule-conferences/), inventeada por [Eric Holscher](https://www.ericholscher.com/), y dice:

    Cuando asistes cómo un grupo de personas, siempre deja espacio para que una persona se una a tu grupo

Cómo un Pac-Man.

![Pac-Man](https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Pacman.svg/456px-Pacman.svg.png)

Dejar espacio para nuevas personas cuando asistes en grupo es una forma física de mostrar un ambiente acogedor e inclusivo.

## Conclusión
A través de este artículo, aprendiste algunas formas de promover tu proyecto y hacer que las personas se interesen en contribuir en tu proyecto.