---
title: "Desplegar un Clúster de MongoDB con Docker Compose"
date: 2024-01-06
description: "Cómo desplegar un cluster de MongoDB usando Docker Compose"
summary: "Aprende a desplegar un cluster de MongoDB con replicación habilitada"
tags: ["tutorial", "docker", "linux", "mongodb"]
---

De un [artículo](https://mariog.dev/blog/desplegar-un-cluster-de-mongodb-con-docker/) previo, si quieres desplegar un clúster de MongoDB en un entorno de desarrollo local, tienes que seguir los pasos a continuación:

* Crear una red de Docker

```
$ docker network create mongodbCluster
```

* Crear un nodo primario y agregarlo a la red que se creó anteriormente

```
$ docker run -d -p 27017:27017 --name mongo1 --network mongodbCluster mongo:6 mongod --replSet myReplicaSet --bind_ip localhost,mongo1
```

3. Una vez que se crea el nodo primario, los nodos secundarios del conjunto de réplica deben crearse

```
$ docker run -d -p 27018:27017 --name mongo2 --network mongodbCluster mongo:6 mongod --replSet myReplicaSet --bind_ip localhost,mongo2
```

```
$ docker run -d -p 27019:27017 --name mongo3 --network mongodbCluster mongo:6 mongod --replSet myReplicaSet --bind_ip localhost,mongo3
```

4. Y finalmente, iniciar el conjunto de réplica

```
$ docker exec -it mongo1 mongosh --eval "rs.initiate({
 _id: \"myReplicaSet\",
 members: [
   {_id: 0, host: \"mongo1\"},
   {_id: 1, host: \"mongo2\"},
   {_id: 2, host: \"mongo3\"}
 ]
})"
```

También puedes usar [Docker Compose](https://docs.docker.com/compose/) para desplegar el clúster, sin tener que escribir todos los comandos anteriores.

A través de este artículo, aprenderás a usar Docker Compose para deplegar un clúster de MongoDB con replicación habilitada.

## Docker Compose
Primero, crea el directorio del proyecto

```
$ mkdir mongodb-cluster
```

En seguida, crea el archivo `compose.yaml` con el siguiente contenido:

```
services:
  mongo1:
    image: mongo:6
    hostname: mongo1
    container_name: mongo1
    ports:
      - 27017:27017
    entrypoint: ["mongod", "--replSet", "myReplicaSet", "--bind_ip", "localhost,mongo1"]
  mongo2:
    image: mongo:6
    hostname: mongo2
    container_name: mongo2
    ports:
      - 27018:27017
    entrypoint: ["mongod", "--replSet", "myReplicaSet", "--bind_ip", "localhost,mongo2"]
  mongo3:
    image: mongo:6
    hostname: mongo3
    container_name: mongo3
    ports:
      - 27019:27017
    entrypoint: ["mongod", "--replSet", "myReplicaSet", "--bind_ip", "localhost,mongo3"]
  mongosetup:
    image: mongo:6
    depends_on:
      - mongo1
      - mongo2
      - mongo3
    volumes:
      - .:/scripts
    restart: "no"
    entrypoint: [ "bash", "/scripts/mongo_setup.sh"]
```

1. Tres servicios son desplegados, `mongo1`, `monogo2`, `mongo3`, y `mongosetup`
2. La imagen `mongo:6` se usa como base para crear los contenedores
3. Los nombres de host se asignan a los contenedores que serán parte del clúster, `mongo1`, `monogo2`, y `mongo3`
4. Se asigna nombre a los contenedores, `mongo1`, `monogo2`, y `mongo3`
5. Se definen los puertos en el host, y se encargarán de redirigir todas las peticiones a los puertos en los contenedores
6. La opción `entrypoint` se usa para especificar el comando que se ejecutará una vez que se inicie el contenedor
7. El servicio `mongosetup` creará el contenedor que se usa para iniciar el conjunto de réplica

Una vez que el archivo `compose.yaml` se ha creado, crea el directorio `scripts`, y un script de BASH (`mongo_setup.sh`), dentro de ese directorio, con el siguiente contenido:

```
$ mkdir scripts
$ touch scripts/mongo_setup.sh
```

```
#!/bin/bash
sleep 10

mongosh --host mongo1:27017 <<EOF
  var cfg = {
    "_id": "myReplicaSet",
    "version": 1,
    "members": [
      {
        "_id": 0,
        "host": "mongo1:27017",
        "priority": 2
      },
      {
        "_id": 1,
        "host": "mongo2:27017",
        "priority": 0
      },
      {
        "_id": 2,
        "host": "mongo3:27017",
        "priority": 0
      }
    ]
  };
  rs.initiate(cfg);
EOF
```

El script anterior iniciará el conjunto de réplica.

Ahora ejecuta el siguiente comando para desplegar el clúster:

```
$ docker compose up --wait
```

Tu clúster de MongoDB se ha desplegado, y está listo para usarse. Si por alguna razón necesitas eliminar el clúster, ejecuta:

```
$ docker compose down
```

## Conclusión
A través de este artículo, aprendiste a desplegar un clúster de MongoDB con replicación habilitada, usando Docker Compose.