---
title: "Ejecutar Percona Backup for MongoDB con Docker"
date: 2023-12-28
description: "Cómo ejecutar Percona Backup for MongoDB usando Docker"
summary: "Aprende a desplegar una instancia de Percona Backup for MongoDB usando Docker"
tags: ["tutorial", "docker", "linux", "mongodb"]
series: ["Percona Backup for MongoDB en un Entorno de Desarrollo con Docker"]
series_order: 3
---

Este es el tercer artículo de la serie sobre Percona Backup for MongoDB (PBM) que incluye los siguientes artículos:

1. Desplegar un Clúster de MongoDB con Docker
2. Almacenamiento de Objectos con MinIO en Docker
3. Ejecutar Percona Backup for MongoDB con Docker

A través de este artículo aprenderás a configurar PBM para respaldar tus bases de datos usando MinIO como la solución de almacenamiento. Como en los artículos previos, se usará Docker para configurar PBM en un entorno de desarrollo.

De [la documentación](https://docs.percona.com/percona-backup-mongodb/installation.html), puedes instalar PBM en cualquiera de laa siguientes maneras:

* Instalar desde los repositorios de Percona repositories usando el gestor de paquetes de tu sistema operativo
* Ejecutar en Docker
* Desplegar en Kubernetes
* Construir desde el código fuente
* Descargar manualmente

## Ejecutar PBM en Docker
### Crear el usuario de PBM

Sigue las instrucciones de [la documentación](https://docs.percona.com/percona-backup-mongodb/install/configure-authentication.html) y crea un usuario para PBM.

Inicia sesión en el nodo primario de MongoDB

```
$ docker exec -it mongo1 mongosh
```

Crea el rol que permite realizar cualquier acción en todos los recursos.

```
db.getSiblingDB("admin").createRole({ "role": "pbmAnyAction",
      "privileges": [
         { "resource": { "anyResource": true },
           "actions": [ "anyAction" ]
         }
      ],
      "roles": []
   });
```

Crea el usuario y asigna el rol que creaste anteriormente.

```
db.getSiblingDB("admin").createUser({user: "pbmuser",
       "pwd": "secretpwd",
       "roles" : [
          { "db" : "admin", "role" : "readWrite", "collection": "" },
          { "db" : "admin", "role" : "backup" },
          { "db" : "admin", "role" : "clusterMonitor" },
          { "db" : "admin", "role" : "restore" },
          { "db" : "admin", "role" : "pbmAnyAction" }
       ]
    });
```

### Iniciar el Contenedor
Inicia el contenedor de PBM como se indica a continuación:

```
$ docker run --name pbm --network mongodbCluster -e PBM_MONGODB_URI="mongodb://pbmuser:secretpwd@<HOST>:<PORT>" -d percona/percona-backup-mongodb:latest
```

Donde:

* `pbm` es el nombre que quieres asignar al contenedor.
* Este contenedor se agrega a la red previamente creada, `mongodbCluster`.
* `PBM_MONGODB-URI` es una cadena que corresponde a un URI de conexión de MongoDB y que se requiere para realizar la conexión al nodo primario del clúster.
* El valor de `<HOST>` el nombre del host (`mongo1`) de tu nodo primario, el valor de `<PORT>` será `27017`.

### Configurar PBM
Creat el archivo `pbm_config.yaml` con el siguiente contenido:

```
storage:
  type: s3
  s3:
    endpointUrl: "http://<HOST>:9000"
    region: us-east-1
    bucket: <BUCKET_NAME>
    prefix: data/pbm/test
    credentials:
      access-key-id: accessKey
      secret-access-key: secretKey
```

Donde:

* <HOST> es el nombre del host del contenedor ejecutando PBM.
* `<BUCKET_NAME>` es el nombre (`mongo-backup`) del bucket creado cuando se configuró MinIO.
* Los valores de `acces-key-id` y `secret-access-key` pueden obtenerse del archivo `credentials.json` descargado después de crear el access key para el usuario de MinIO. 

En seguida, copia el archivo al contenedor:

```
$ docker cp pbm_config.yaml container_name:/tmp/pbm_config.yaml
```

`container_name` es el nombre (`pbm`) del contenedor de PBM.

Inicia sesión en el contenedor:

```
$ docker exec -it pbm bash
```

Y ejecuta:

```
$ pbm config --file /tmp/pbm_config.yaml
```

### Configurar `pbm-agent`
Antes de realizar respaldos, debes iniciar `pbm-agent` en cada servidor ejecutando `mongod`.

Para hacer esto, debes iniciar sesión en cada nodo del clúster, instalar PBM e iniciar `pbm-agent`:

```
$ docker exec -it <NODE> bash
```

Reemplaza `<NODE>` con mongo1, mongo2, y mongo3. Dentro de cada nodo debes ejecutar los siguientes comandos.

Instala todos los paquetes requeridos:

```
$ apt update && apt install curl cron nano && curl -O https://repo.percona.com/apt/percona-release_latest.generic_all.deb && apt install -y gnupg2 lsb-release ./percona-release_latest.generic_all.deb && apt update && percona-release enable pbm release && apt update && apt install -y percona-backup-mongodb
```

El comando anterior instalará PBM.

Y finalmente, define la variable de entorno `PBM_MONGODB_URI` y ejecuta `pbm-agent`.

```
PBM_MONGODB_URI="mongodb://pbmuser:secretpwd@<HOST>:27017/?authSource=admin" && export PBM_MONGODB_URI="mongodb://pbmuser:secretpwd@<HOST>:27017/?authSource=admin&replSetName=myReplicaSet" && pbm-agent
```

Donde `<HOST>` es el nombre del host (`mongo1`) del nodo primario.

Una vez que PBM está ejecutándose en cada nodo del clúster, puedes realizar respaldos.

### Iniciar un Respaldo
Puedes hacer un respaldo completo de las bases de datos y colecciones en tu servidor de MongoDB ejecutando el siguiente comando desde el contenedor `pbm`:

```
$ docker exec -it pbm bash
```

```
pbm backup --type=TYPE
```

Puedes especificar el tipo de respaldo que deseas realizar: `physical` o `logical`.

Cuando el tipo de respaldo seleccionado es `physical`, Percona Backup for MongoDB copia el contenido del directorio `dbpath` (archivos de datos y metadatos, índices, journal, y registros) de cada nodo y la configuración del servidor correspondiente al conjunto de réplicas al almacenamiento.

En respaldos de tipo `logical`, Percona Backup for MongoDB copia los datos directamente al almacenamiento. Cuando no se pasa la opción `--type`, Percona Backup for MongoDB hace un respaldo lógico.

Para listar todos los respaldos disponibles en el almacenamiento, ejecuta el siguiente comando:

```
pbm list
```

Obtendrás la siguiente salida:

```
Backup snapshots:
  2023-03-10T19:32:35Z <logical> [restore_to_time: 2023-03-10T19:32:40Z]
```

Para más información, revisa [la documentación](https://docs.percona.com/percona-backup-mongodb/usage/start-backup.html).

#### Respaldo y Restauración Selectivos
Cuando ejecutas `pbm backup`, obtienes un respaldo completo de las bases de datos en tu servidor. Pero si quieres obtener el respaldo de una base de datos o una colección, puedes intentar realizar un respaldo y restauración selectivos.

Por ejemplo, si tienes algunas bases de datos en tu servidor, y necesitas respaldar solo una base de datos o colección de tu servidor, puedes realizar un respaldo selectivo ejecutando cualquiera de los siguientes comandos:

```
pbm backup --ns=staff.Payments
```

```
pbm backup --ns=Invoices.*
```

Con el primer comando, obtendrás un respaldo de la colección `Payments` de la base de datos `staff`.

Con el segundo comando, obtendrás un respaldo de la base de datos `Invoices` y todas las colecciones en ella.

Si ejecutas `pbm list`, obtendrás la siguiente salida:

```
Backup snapshots:
  2023-03-10T19:49:42Z <logical, selective> [restore_to_time: 2023-03-10T19:49:46Z]
```

Para restaurar una base de datos o colección específicas, ejecuta el comando `pbm restore`:

```
pbm restore <backup_name> --ns <database.collection>
```

Donde `<backup_name>` es el nombre del respaldo (i.e. `2023-03-10T19:49:42Z`) y `<database.collection>` es el nombre de la base de datos y colección que quieres restaurar.

Para más información sobre respaldo y restauración selectivos, revisa esta página en [la documentación](https://docs.percona.com/percona-backup-mongodb/usage/start-backup.html).

## Conclusión
Después de configurar un clúster de MongoDB con tres nodos y replicación habilitada, MinIO y Percona Backup for MongoDB, ya puedes usar estas herramientas en tu entorno de desarrollo local.