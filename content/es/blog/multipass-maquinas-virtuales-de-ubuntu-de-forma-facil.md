---
title: "Multipass: Máquinas Virtuales de Ubuntu de Forma Fácil"
date: 2023-11-15
description: "Cómo configurar máquinas virtuales de Ubuntu de forma fácil con Multipass"
summary: "Aprende a usar Multipass para configurar máquinas virtuales de Ubuntu"
tags: ["tutorial","ubuntu", "virtualization"]
---

A través de los años, he usado diferentes herramientas de virtualización, principalmente Open Source, como una forma de probar otras distribuciones de GNU/Linux, configurar entornos de desarrollo, o para enseñar sobre conceptos de diferentes temas.

Entre estas herramientas se encuentra [Oracle VM VirtualBox](https://virtualbox.org) (que he usado desde antes de la compra de Sun Microsystems por parte Oracle), [VMWare Workstation Player](https://www.vmware.com/content/vmware/vmware-published-sites/us/products/workstation-player.html.html), y [QEMU](https://www.qemu.org/), pero el año pasado descubrí Multipass.

![El Quinto Elemento - Escena Multipass](https://media.giphy.com/media/uIGHPjEfdc0Ni/giphy.gif)

No, no el de la escena de [El Quinto Elemento](https://es.wikipedia.org/wiki/El_quinto_elemento) con [Milla Jovovich](https://es.wikipedia.org/wiki/Milla_Jovovich) como Leeloo.

## ¿Qué es Multipass?

[Multipass](https://multipass.run/docs) es una herramienta para generar máquinas virtuales de Ubuntu tipo nube de forma rápida en GNU/Linux, macOS, y Windows. Proporciona una simple pero poderosa interfaz de línea de comandos, que permite acceder rápidamente a Ubuntu o crear tu propia mini nube local.

Usa KVM en GNU/Linux, Hyper-V en Windows y QEMU en macOS para ejecutar la máquina virtual con una sobrecarga mínima. También se puede usar VirtualBox en Windows y macOS. Multipass obtendrá las imágenes por ti y las mantendrá actualizadas.

Desarrollado por [Canonical](https://canonical.com).

## Instalación
Multipass está disponible como un paquete [Snap](https://snapcraft.io/about) en GNU/Linux. El [demonio de snap](https://snapcraft.io/docs/installing-snapd) (`snapd`) se encuentra preinstalado en las siguientes distribuciones:

- [KDE Neon](https://neon.kde.org/)
- [Manjaro](https://manjaro.org/)
- [Solus](https://getsol.us/)
- [Ubuntu](https://ubuntu.com/) 18.04 y superior
- La mayoría de las versiones de [Ubuntu](https://wiki.ubuntu.com/DerivativeTeam/Derivatives#Official_Ubuntu_Flavors)
- [Zorin OS](https://zorinos.com/)

En caso contrario, sigue las instrucciones para tu distribución en la [documentación](https://snapcraft.io/docs/installing-snapd).

Una vez que `snapd` está disponible en tu sistema, puedes instalar Multipass ejecutando el siguiente comando:

```bash
$ snap install multipass
```

Para arquitecturas distintas a `amd64`, necesitarás el canal `beta` al momento.

También puedes usar el canal `edge` para obtener la última versión de desarrollo:

```bash
$ snap install multipass --edge
```

Si quieres desinstalar Multipass, ejecuta:

```bash
$ snap remove multipass
```

Para instalar macOS, sigue las instrucciones en la [documentación](https://multipass.run/docs/installing-on-macos).

La documentación también contiene instrucciones para instalar en [Windows](https://multipass.run/docs/installing-on-windows).

## Crea una Instancia

Para crear una instancia, ejecuta:

```bash
$ multipass launch
```

El comando anterior creará una instancia usando una imagen de la última versión LTS de Ubuntu (`22.04.3`). Multipass asignará un nombre aleatorio a la instancia y aplicará la configuración por defecto.

Puedes listar todas las instancias en tu sistema ejecutando:

```bash
$ multipass list
Name                    State             IPv4             Image
visionary-longspur      Running           10.81.157.45     Ubuntu 22.04 LTS
```

Puedes obtener los detalles de tu instancia con el siguiente comando:

```bash
$ multipass info visionary-longspur
Name:           visionary-longspur
State:          Running
IPv4:           10.81.157.45
Release:        Ubuntu 22.04.3 LTS
Image hash:     054db2d88c45 (Ubuntu 22.04 LTS)
CPU(s):         1
Load:           2.30 0.82 0.29
Disk usage:     1.4GiB out of 4.8GiB
Memory usage:   157.2MiB out of 951.9MiB
Mounts:         --
```

Por defecto, Multipass asigna los siguientes recursos:

- 1 CPU
- 5GB de disco
- 1GB de RAM

El usuario por defecto es `ubuntu`.

## Crea una Instancia Personalizada

Puedes crear una instancia personalizada. Primero, revisa que imágenes están disponibles:

```bash
$ multipass find
Image                       Aliases           Version          Description
core                        core16            20200818         Ubuntu Core 16
core18                                        20211124         Ubuntu Core 18
core20                                        20230119         Ubuntu Core 20
core22                                        20230717         Ubuntu Core 22
20.04                       focal             20231011         Ubuntu 20.04 LTS
22.04                       jammy,lts         20231026         Ubuntu 22.04 LTS
23.04                       lunar             20231025         Ubuntu 23.04
23.10                       mantic,devel      20231011         Ubuntu 23.10
appliance:adguard-home                        20200812         Ubuntu AdGuard Home Appliance
appliance:mosquitto                           20200812         Ubuntu Mosquitto Appliance
appliance:nextcloud                           20200812         Ubuntu Nextcloud Appliance
appliance:openhab                             20200812         Ubuntu openHAB Home Appliance
appliance:plexmediaserver                     20200812         Ubuntu Plex Media Server Appliance

Blueprint                   Aliases           Version          Description
anbox-cloud-appliance                         latest           Anbox Cloud Appliance
charm-dev                                     latest           A development and testing environment for charmers
docker                                        0.4              A Docker environment with Portainer and related tools
jellyfin                                      latest           Jellyfin is a Free Software Media System that puts you in control of managing and streaming your media.
minikube                                      latest           minikube is local Kubernetes
ros-noetic                                    0.1              A development and testing environment for ROS Noetic.
ros2-humble                                   0.1              A development and testing environment for ROS 2 Humble.
```

Crea una instancia con la siguiente configuración:

- Ubuntu Mantic Minotaur (`23.10`)
- Nombre personalizado: `server`
- 2 CPUs
- 10GB de disco
- 2 GB de RAM

```bash
$ multipass launch mantic --name server --cpus 2 --disk 10G --memory 2G
```

Donde `mantic` es el nombre de la imagen que será usada para crear la instancia.

## Comandos Básicos
### Abre Una Shell Dentro de la Instancia
Para abrir una shell en una instancia existente `server`, ejecuta el siguiente comando:

```bash
$ multipass shell server
Welcome to Ubuntu 22.04.3 LTS (GNU/Linux 5.15.0-87-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Fri Nov 10 01:32:53 CST 2023

  System load:  0.0               Processes:             90
  Usage of /:   30.9% of 4.67GB   Users logged in:       0
  Memory usage: 19%               IPv4 address for ens3: 10.81.157.45
  Swap usage:   0%


Expanded Security Maintenance for Applications is not enabled.

0 updates can be applied immediately.

Enable ESM Apps to receive additional future security updates.
See https://ubuntu.com/esm or run: sudo pro status


The list of available updates is more than a week old.
To check for new updates run: sudo apt update

To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.
```

La instancia inicia automáticamente si se encuentra detenida o suspendida.

Puedes ejecutar comandos como harías en cualquier instalación de Ubuntu.

### Ejecuta Un Comando Dentro De Una Instancia
Los comandos pueden ejecutarse dentro de una instancia directamente desde el host, ejecutando el siguiente comando:

```bash
$ multipass exec server -- pwd
/home/ubuntu
```

### Transfiere Un Archivo Desde el Host a la Instancia
Si quieres transferir un archivo, como un script de Bash, desde el host a la instancia, ejecuta el siguiente comando:

```bash
$ multipass transfer install.sh server:/home/ubuntu/install.sh
```

`install.sh` es el archivo del host que será transferido a la instancia.

`server` es el nombre de la instancia.

`/home/ubuntu/install.sh` es la ruta dentro de la instancia donde el archivo será almacenado.

### Cambia el Estado de la Instancia
#### Detiene Una Instancia
Ejecuta el siguiente comando para detener una instancia:

```bash
$ multipass stop server
```

Puedes detener tantas instancias como necesites con un solo comando:

```bash
$ multipass stop server server2 server3
```

Todas las instancias ejecutádonse pueden ser detenidas al mismo tiempo:

```bash
$ multipass stop --all
```

#### Inicia Una Instanceia
Ejecuta el siguiente comando para iniciar una instancia:

```bash
$ multipass start server
```

Puedes iniciar tantas instancias como necesites con un solo comando:

```bash
$ multipass start server server2 server3
```

Todas las instancias detenidas o suspendidas pueden iniciarse al mismo tiempo:

```bash
$ multipass stop --all
```

#### Suspende Una Instancia
Ejecuta el siguiente comando para suspender una instancia:

```bash
$ multipass suspend server
```

Puedes suspender tantas instancias como necesites con un solo comando:

```bash
$ multipass suspend server server2 server3
```

Toas las instancias ejecutándose pueden suspenderse al mismo tiempo:

```bash
$ multipass suspend --all
```

### Elimina Una Instancia
Puedes eliminar una instancia ejecutando el siguiente comando:

```bash
$ multipass delete server
```

O tantas instancias como necesites:

```bash
$ multipass delete server server2 server3
```

Las instancias ejecutándose serán detenidas y eliminadas.

Después de eliminar una instancia, ejecuta:

```bash
$ multipass purge
```

El comando `multipass purge` eliminará permanentemente todas las instancias eliminadas con el comando `multipass delete`. Esto destruirá todo rastro de la instancia y no puede deshacerse.

Aprendiste a usar Multipass y ya puedes crear máquinas virtuales de Ubuntu.
