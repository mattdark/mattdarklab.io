---
title: "Una Guía Para Contribuir Al Código Abierto Sin Tener Conocimientos Técnicos"
date: 2024-01-26
description: "Comparto algunas recomendaciones de como contribuir a proyectos de código abierto sin tener conocimientos técnicos"
summary: "En este texto aprenderás como contribuir al código abierto sin tener un perfil técnico"
tags: ["community"]
---

¿Qué es lo primero que viene a tu mente cuando escuchas "Contribuciones al Código Abierto"? Probablemente estás pensando que tienes que aprender algún lenguaje de programación, o tienes que estudiar una carrera en tecnología, o necesitas un perfil técnico, pero lo cierto es que cualquiera puede contribuir.

Mi primer acercamiento al código abierto fue en 2006, cuando instalé Ubuntu, la primera distribución GNU/Linux que usé, y mis primeras contribuciones no ocurrieron hasta unos años más tarde.

En 2008, me invitaron a ser conferencista en un evento local, donde presenté una charla para hablar de mi experiencia usando GNU/Linux, y he estado participando en eventos desde entonces para promover los proyectos en los que he participado, y las herramientas y tecnologías que he usado.

Ser conferencistas en eventos ha sido una forma de contribuir al código abierto, pero también he contribuido en áreas como localización, escritura técnica, desarrollo de software y organización de eventos.

Aunque soy desarrollador de software y estudié una carrera en tecnología, no significa que esos son requisitos para contribuir o construir una carrera en el código abierto.

A través de este artículo, aprenderás como contribuir al código abierto sin tener un perfil técnico.

## Cómo Contribuir
Si encuentres un proyecto que ames y quieres ayudar, considera lo siguiente:

* No se necesita conocimiento de programación
* No es necesario instalar otro sistema operativo o familiarizarse con la línea de comandos
* No tienes que aprender otro idioma
* Tener experiencia no es un requisito, pero si que te emocione aprender y compartir

Y algunas de las áreas donde puedes contribuir además del desarrollo de software son:
* Diseño
* Documentación
* Localización/Traducción
* Soporte
* Mentoría
* Evangelismo

## Diseño
A través de los años he aprendido a usar herramientas de diseño de código abierto como [Inkscape](https://inkscape.org/), [GIMP](https://www.gimp.org/) y [Krita](https://krita.org/en/). Aunque no soy un experto en esta área, he usado estas herramientas para crear gráficos para algunos proyectos personales, y recientemente el logo de [Let's Talk! Open Source](https://instagram.com/letstalkoss), que creé usando Inkscape.

Puedes encontrar algunas ideas de [como contribuir](https://opensource.guide/how-to-contribute/) al código abierto siendo diseñador en las [Guías de Código Abierto](https://opensource.guide) de [GitHub](https://github.com).

* Reestructurar los diseños para mejorar la usabilidad del proyecto
* Realizar investigaciones de usuarios para reorganizar y perfeccionar la navegación o los menús del proyecto, [como sugiere Drupal](https://www.drupal.org/community-initiatives/drupal-core/usability)
* Elaborar una guía de estilo para ayudar a que el proyecto tenga un diseño visual consistente
* Crear arte para camisetas o un nuevo logotipo, [como hicieron los contribuidores de hapi.js’s](https://github.com/hapijs/contrib/issues/68)

Pero, ¿los diseñadores pertenecen al código abierto? Dejaré que Veethika Mishra, Diseñadora de Producto Senior en [GitLab](https://about.gitlab.com/), responda esta pregunta a través de [este artículo](https://blog.prototypr.io/do-designers-belong-in-open-source-160bb97e3b78).

## Documentación
Puedes contribuir a la documentación de un proyecto mejorando el contenidoYou can contribute to the documentation of a project by improving the content. Al explorar la documentación, puedes encontrar errores tipográficos, información errónea, instrucciones incorrectas o información desactualizada. Puedes reportar las páginas que necesitan mejora contactando a los mantenedores del proyecto o hacer esos cambios tú mismo, creando un [pull request](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/proposing-changes-to-your-work-with-pull-requests/about-pull-requests) o [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) en el repositorio de Git.

Si no conoes Git, puedes seguir estas guías:

* [Creating merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
* [Creating a pull request](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/proposing-changes-to-your-work-with-pull-requests/creating-a-pull-request)

Aquí hay algunos ejemplos de pull requests que creé en la documentación de Persona, cuando trabajaba ahí:

* [Correcting the Selective backup commands](https://github.com/percona/pbm-docs/pull/82)

* [Updating documentation](https://github.com/percona/psmdb-docs/pull/648)

* [Adding a note on dependencies](https://github.com/percona/pdmysql-docs/pull/51)

No todas las respuestas para cada pregunta que tengas sobre una tecnología o herramienta las vas a encontrar en la documentación oficial. Si resolviste un problema que no estaba bien documentado, y te gusta escribir, crear artículos y tutoriales es otra forma de contribuir a un proyecto de código abierto.

## Localización/Tranducción
De acuerdo a [este artículo](https://phrase.com/blog/posts/software-localization/#what-is-software-localization) de [Phrase](https://phrase.com), la localización de software es un proceso en el desarrollo de software que busca adaptar una web o aplicación móvil a la cultura e idioma de los usuarios en el mercado objetivo.

La localización va más allá de una simple traducción. Mientras que la traducción se enfoca en transferir palabras entre idiomas, la localización implica adaptar imágenes, colores, formatos de fecha y hora, elementos de UI, y más para hacer que un producto de software se vea y sienta tan nativo como sea posible en el mercado objetivo.

Como lo he mencionado anteriormente, en otros artículos, el inglés es mi segundo idioma y empecé a aprenderlo hace 20 años, cuando estaba en bachillerato. Tuve la oportunidad de tomar clases de inglés en una universidad local por 4 años y medio.

Cuando decidí estudiar una carrera en tecnología, ya hablaba inglés, lo que me puso en una posición de privilegio. En América Latina, no todos tienen acceso a estudiar inglés como segundo idioma.

Cuando me uní a la comunidad de Mozilla en 2011, ya era desarrollador de software, pero preferí contribuir a la localización de Firefox, ya que pensé que era más valioso ayudar a localizar el navegador a mi idioma nativo, español.

Mozilla usa [Pontoon](https://github.com/mozilla/pontoon), un sistema de gestión de traducciones usado y desarrollado por la [comunidad de localización de Mozilla](https://pontoon.mozilla.org/). Se especializa en localización de código abierto impulsada por la comunidad y utiliza sistemas de control de versiones para almacenar traducciones.

Puedes dar un vistazo a los diferentes proyectos de localización [aquí](https://pontoon.mozilla.org). 

Cuando hablamos de documentación, la mayor parte del contenido sobre código abierto, desarrollo de software o cualquier tema relacionado a tecnología está disponible en inglés y aunque hay esfuerzos de localizar herramientas y traducir la documentación a otros idiomas, es importante que como mantenedores de proyectos, creadores de contenido o conferencistas, debemos asegurar que la documentación o cualquier contenido creado también esté disponible en nuestro idioma nativo.

La información más reciente sobre cualquier tecnología o herramienta está disponible en inglés, y las traducciones a otros idiomas pueden tomar años, especialmente si se trata de libros. En ese caso, si hablas inglés como segundo idioma, podrías ayudar a traducir la documentación oficial de tu proyecto favorito a tu idioma nativo, como lo está haciendo la comunidad de Python con la traducción de la documentación a diferentes idiomas. Puedes encontrar más información sobre este proyecto en la [Python Developer's Guide](https://devguide.python.org/documentation/translating/).

Empecé a escribir en DEV en 2019, con la mayor parte del contenido en inglés, pero desde 2023, cada artículo publicado ahí tambien está disponible en español en [mi sitio web](https://mariog.dev).

## Soporte
Si tienes poco tiempo, puedes ayudar a otros:

* Respondiendo preguntas en:
    * Stack Overflow
    * Reddit
    * Forums
    * Blogs
    * Social media
* Respondiendo preguntas en una conversación uno a uno
* Ayuda a resolver problemas
* Recomienda alternativas de código abierto

No necesitas ser un experot para proporcionar soporte, puedes compartir lo que has aprendido de usar herramientas de código abierto, o contribuyendo a algún proyecto.

## Mentoría
De mi artículo sobre [Mis Reglas Para Ser Conferencista Técnico](https://mariog.dev/blog/mis-reglas-para-ser-conferencista-tecnico/), como mentor de algunos proyectos de Código Abierto y personas, he identificado tres razones principales de porque deberías proporcionar mentoría:

* Promueve el desarrollo personal y profesional. Las mentorías pueden ser una oportunidad para el mentor de identificar:
    * Que tecnologías le apasionan
    * Área de experiencia
    * Habilidades que necesita mejorar o adquirir

* Mejorar tus habilidades de comunicación. Preparar una sesión de mentoría implica no solo tener conocimiento sobre el tema, puede ser difícil explicar conceptos técnicos de manera clara y concisa, sin que sea más complejo de entender para personas con menos experiencia o sin conocimientos técnicos.

* Mejorar tus habilidades de liderazgo. Un mentor es una guía para que las personas tengan éxito en sus metas, no importa lo que quieran lograr, como ser mejor desarrollador o tener éxito en su primera conferencia. Para tus aprendices: 
    * Serás una fuente de inspiración
    * Les mostrarás el camino a seguir
    * Proporcionarás el soporte y las herramientas que necesitan para alcanzar sus metas

    Y eso es lo que hace a un [buen líder](https://www.betterup.com/blog/what-is-a-leader-and-how-do-you-become-one).

* Expandir conexiones y redes. Proporcionar mentoría puede ser una oportunidad para conocer personas de todo el mundo y conocer diferentes culturas, así como un espacio para futuras colaboraciones.

* Aprendizaje mutuo. Los mentores son personas con conocimiento en su área de experiencia, pero no lo saben todo. En una relación de mentoría, ambas partes deben estar abiertas a aprender entre si. Nuevas tecnologías pueden descubrirse en una sesión de mentoría, y eso puede ser útil para futuros proyectos.

## Evangelism
¿Quieres promover tu proyecto de código abierto favorito? Dependiendo de tu disponibilidad, aquí hay algunas opciones que debes considerar:

* Organiza eventos
    * Meetups
    * Conferencias
    * Talleres
    * Hackathons
    * Webinars
* Se un conferencista
* Comparte el proyecto
    * Listas de correo
    * Redes sociales
    * Aplicaciones de mensajería

La idea detrás de organizar eventos es que más personas conozcan sobre un proyecto específico o sobre el código abierto en general, y como contribuir, sin importar el perfil o la experiencia que los participantes puedan tener. No tiene que ser un evento, los eventos pequeños son un buen punto de partida. Organizar un evento requiere tiempo, y mientras más personas se involucren es mejor, ya que no tienes que encargarte de todo.

El formato del evento dependerá de cuantas esperes que asistan, el tamaño del lugar, la fecha y la hora, y cuanto tiempo puedes dedicar. La mayoría de los eventos sobre código abierto son organizados por voluntarios. También puedes unirte al equipo de un evento que ya se organice en tu ciudad.

Los webinars también son una buena opción a considerar. La ventaja de los eventos virtuales es que más personas pueden tener acceso.

¿Te gustaría mejorar tus habilidades para hablar en público? Considera ser conferencista y participar en eventos locales, y compartir tu experiencia con el código abierto. Lee el artículo que escribí sobre [Mis Reglas Para Ser Conferencista Técnico](https://mariog.dev/blog/mis-reglas-para-ser-conferencista-tecnico/).

## Conclusión
Basado en mi experiencia previa, he recomendado algunas áreas en las que puedes contribuir sin que necesites tener un perfil técnico. Además, contribuir al código abierto es una oportunidad que debes considerar ya que puedes aprender, adquirir nuevas habilidades, conocer personas, y ganar la experiencia que necesitas para construir tu carrera.