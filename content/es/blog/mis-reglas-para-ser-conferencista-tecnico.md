---
title: "Mis Reglas Para Ser Conferencista Técnico"
date: 2024-01-13
description: "Comparto algunas de las reglas que he seguido para ser conferencista técnico"
summary: "En este texto aprenderás sobre lo que me ha funcionado para ser conferencista técnico"
tags: ["community"]
---

## 1. No Necesitas Tener Experiencia Pero Si Querer Compartir
La primera vez que entré en una habitación para estar frente a otras personas y compartir todo lo que sabía sobre GNU/Linux, no era el experto que soy hoy.

Había empezado a usar GNU/Linux en diciembre de 2006, cuando estaba en la universidad, y apenas sabía como instalar Ubuntu junto a Windows o usar la terminal, y había enseñado a algunas personas como configurar el sistema operativo.

En abril de 2008, un amigo me invitó a participar en el [Festival Latinoamericano de Instalación de Software Libre](https://flisol.info). Estuve ahí para hablar de mi experiencia usando Linux. 

Hablar en público no era algo en lo que tuviera mucha experiencia en ese momento, y me preguntaba si era la persona correcta para hacerlo o si tenía algo que decir.

Una herramienta que aprendiste a usar recientemente, un problema que resolviste escribiendo código en cualquier lenguaje de programación, algo que descubriste mientras asistías a una conferencia, cualquier conocimiento que tengas vale la pena compartirlo.

Incluso si no tienes la experiencia que piensas que necesitas. Cualquier conferencista que conociste en un evento empezó de esa manera, sin poseer algún conocimiento previo o experiencia en el tema.

## 2. Encuentra un Mentor
Cuando aplicas a un llamado de propuestas para participar en un evento de tecnología, esta es la información que debes preparar sobre tu propuesta:

* Elegir un tema
* Elegir un título para tu charla o taller
* Escribir una descripción
* Escribir un resumen, incluyendo un temario

Los temas que abordarás a través de tu presentación, así como el tiempo que les dedicarás, incluyendo espacio para demos y sesión de preguntas y respuestas, dependerá del tiempo asignado por la organización del evento.

Escribir propuestas no es fácil la primera vez. Eventos como [PyCon US](https://us.pycon.org) tienen programas de [mentoría](https://us.pycon.org/2024/speaking/mentorship/), y puedes solicitar que conferencistas con experiencia te guíen para preparar tu propuesta.

Me uní al programa [GitLab Heroes](https://about.gitlab.com/community/heroes/) en agosto de 2019, y fui conferencista en GitLab Commit tres años seguidos de 2019 a 2021. En 2019, cuando recibí un correo electrónico confirmando que mi propuesta había sido aceptada, GitLab también me asignó a una mentora que me guió para preparar mi presentación y me ayudó a practicar antes del evento.

Soy de México e inglés es mi segundo idioma. Como conferencista, GitLab Commit fue la primera vez que presenté una charla en inglés y mi participación fue un éxito gracias al soporte proporcionado por GitLab. 

Puedes leer más sobre [mi inesperado viaje para convertirme en GitLab Hero](https://dev.to/mattdark/my-unexpected-journey-to-become-a-gitlab-hero-ak1) en DEV.

Encontrar un mentor puede ser difícil, y la persona correcta para guiarte debería ser alguien con experiencia previa hablando en público y/o alguien con experiencia en el tema que discutirás a través de tu presentación, y puedes encontrar a la persona correcta en:

* Profesores
* Amigos
* Alguien de la comunidad tecnológica en la que participas
* Conferencista que conociste en un evento de tecnología
* Alguien que ya sigues en redes sociales
* Plataformas como [Coding Coach](https://codingcoach.io)

No olvides ser amable, respetar su tiempo y ser paciente con quien sea que contactaras para que sea tu mentor.

### Ser un Mentor
Como mentor de algunos proyectos de Código Abierto y personas, he identificado tres razones principales de porque deberías proporcionar mentoría:

* Promueve el desarrollo personal y profesional. Las mentorías pueden ser una oportunidad para el mentor de identificar:
 * Que tecnologías le apasionan
 * Área de experiencia
 * Habilidades que necesita mejorar o adquirir

* Mejorar tus habilidades de comunicación. Preparar una sesión de mentoría implica no solo tener conocimiento sobre el tema, puede ser difícil explicar conceptos técnicos de manera clara y concisa, sin que sea más complejo de entender para personas con menos experiencia o sin conocimientos técnicos.

* Mejorar tus habilidades de liderazgo. Un mentor es una guía para que las personas tengan éxito en sus metas, no importa lo que quieran lograr, como ser mejor desarrollador o tener éxito en su primera conferencia. Para tus aprendices: 
 * Serás una fuente de inspiración
 * Les mostrarás el camino a seguir
 * Proporcionarás el soporte y las herramientas que necesitan para alcanzar sus metas

    Y eso es lo que hace a un [buen líder](https://www.betterup.com/blog/what-is-a-leader-and-how-do-you-become-one).

* Expandir conexiones y redes. Proporcionar mentoría puede ser una oportunidad para conocer personas de todo el mundo y conocer diferentes culturas, así como un espacio para futuras colaboraciones.

* Aprendizaje mutuo. Los mentores son personas con conocimiento en su área de experiencia, pero no lo saben todo. En una relación de mentoría, ambas partes deben estar abiertas a aprender entre si. Nuevas tecnologías pueden descubrirse en una sesión de mentoría, y eso puede ser útil para futuros proyectos.

### Cursos de Oratoria
Como se explica [aquí](https://www.mindtools.com/addwv9h/learning-styles), hay diferentes estilos de aprendizaje, según lo declarado por Walter Burke Barbe y sus colegas, quienes propusieron tres "modalidades" de aprendizaje: *Visual*, *Auditivo*, y *Cinestésico* (movimiento y tacto). A estos se les refiere como VAK (del acrónimo en inglés) para simplificar.

Una variación del acrónimo, desarrollado por el profesor Neil D. Fleming con sede en Nueva Zelanda, es [VARK®](http://vark-learn.com/), o visual, auditivo, lectura/escritura, y cinestésico.

Siempre elijo aprender leyendo y escribiendo. Cuando hay una nueva tecnología o herramienta que quiero probar, primero voy a la documentación, reviso los ejemplos, y ejecuto pruebas en diferentes escenarios. Documento todo lo que aprendo a través de artículos, porque explicar el tema con mis propias palabras, hace que lo entienda mejor.

Aunque miro videos en YouTube, solo lo hago cuando intento corregir un error o implementar una solución, y no encontré un ejemplo útilen la documentación o en una búsqueda de Google.

También soy una persona autodidacta y nunca he tomado un curso de oratoria, pero hay algunos cursos en plataformas como [Coursera](https://www.coursera.org/) o [Grow with Google](https://grow.google/intl/es/courses-and-tools), que puedes checar.

## 3. La Práctica Hace al Maestro
*Practice makes perfect*, es un modismo en inglés que se usa para decir que una persona se vuelve bueno en algo si lo hace frecuentemente, de acuerdo al diccionario [Merriam-Webster](https://www.merriam-webster.com/dictionary/practice%20makes%20perfect). En español el equivalente es "La práctica hace al maestro".

Hablar en público es una habilidad que mejoras en cada iteración. Tener nervios es normal, algunas cosas pueden no salir como esperabas, y tener conocimiento sobre un tema no significa que tienes todas las respuestas. Aprender de lo que salió bien o mal es lo que hace que lo hagas mejor la siguiente vez.

Aquí hay una lista de cosas por hacer que pueden ayudarte a hacerlo mejor en tu próxima conferencia:

* Lee tanto como puedas sobre el tema que discutirás en tu presentación
* Revisa tus diapositivas antes de la presentación, incluso si están listas, y consideras que no hay algo más que agregar. Puedes encontrar errores de ortografía, información erronea o desactualizada, etc.
* Si estás presentando un producto, una herramienta o cualquier tecnología, asegurate que todo lo que necesitas para que funcione esté listo. Si falla, intenta arreglarlo, pero no le dediques mucho tiempo. Puedes arreglarlo después y compartir la solución a través de un artículo o video
* Si estás presentando en un idioma diferente, es normal olvidar lo que tienes que decir, o pensar que tu pronunciación no es tan buena como quieres que sea. Pero este es el primer paso para ser mejor comunicándose en ese idioma, y lo harás mejor cada vez.
* Estar abierto a recibir retroalimentación y aprender de la audiencia
* Tomar notas de lo que salió mal
* No tires la toalla la primera vez que algo salió mal

## 4. Mejores Diapositivas Para Una Mejor Presentación
Si planeas usar diapositivas para tu presentación, aquí hay algunas recomendaciones:

* Palabras clave. En una presentación, es importante que las personas te pongan atención y no a las diapositivas. Las personas no están ahí para leer el contenido de tus diapositivas, sino escuchar lo que tienes que decir sobre el tema. Una mejor forma de organizar el contenido de cada diapositiva sería usar palabras clave, en lugar de escribir parrafos completos.
* Imágenes. Una imagen vale más que mil palabras. Puedes reemplazar palabras con imágenes que representen las ideas que quieres comunicar.
* Agrega un temario. Las diapositivas son una guía para los conferencistas. Agregar un temario podría ayudar a organizar el contenido de tu presentación. Para la audiencia sería una forma de conocer que temas se abordarán y en que orden.
* Escribe un guión. A un actor en una película se le da un guión para conocer sus dialogos, saber como reaccionar en una situación, y cuando realizar una acción. Pero los guiones también pueden usarse para prepararte para una conferencia, independientemente del formato de la presentación, no importa si es una charla, un webinar, o una charla relámpago. Crea un documento donde escribas lo que vas a decir, y usalo para practicar antes de tu presentación.
* Agrega notasa. Las notas pueden ser útiles para recordar ideas asociadas a palabras clave, lo que representa una imagen, información importante que deba mencionarse, instrucciones para demos, etc.

## 5. Usa Las Herramientas Que Se Ajusten A Tus Necesidades
### Diapositivas
A través de los años, he usado diferentes herramientas para preparar las diapositivas de mis presentaciones, incluyendo:

* OpenOffice Impress
* LibreOffice Impress
* Google Slides
* [reveal.js](https://revealjs.com/)

Cuando presenté mi primera charla, había usado Ubuntu por casi un año y encontré alternativas de Software Libre y Código Abierto para las herramientas que ya usaba en Windows. Durante mis primeras conferencias, la herramienta que usé para mis presentaciones fue OpenOffice Impress, que ya fue descontinuada.

Oracle [acquirió Sun Microsystems](https://en.wikipedia.org/wiki/Acquisition_of_Sun_Microsystems_by_Oracle_Corporation), la compañía detrás del desarrollo de OpenOffice, en enero de 2010. Como resultado, un fork llamado LibreOffice fue publicado un año después, en 2011.

En el año del lanzamiento de LibreOffice, empecé a utilizarlo en lugar de OpenOffice. Desde entonces, LibreOffice Impress ha sido una de las herramientas que he usado para la creación de las diapositivas para mis presentaciones.

Muchos eventos proporcionan una plantilla a sus conferencias, que ellos puedes modificar para sus presentaciones. Este documento usualmente se comparte vía Google Slides.

Las ventajas de usar Google Slides son que puedes compartir publicamente tus diapositivas usando el enlace generado por Google, acceder a una presentación en cualquier navegador, y no hay errores de compatibilidad al abrir el documento.

Hay algunos repositorios de GitHub en mi cuenta donde pueden encontrar algunas de mis presentaciones, creadas con [reveal.js](https://revealjs.com/), un framework para crear presentaciones usando HTML y Markdown. 

Si quieres usar reveal.js, solo necesitas un editor y un navegador web. Y puedes compartir la presentación con el mundo usando [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), como harías con cualquier sitio creado con HTML5.

De estas herramientas, ¿cuál deberías elegir? La que ya sabes como usar, a menos que quieras probar otras herramientas, como [present](https://github.com/vinayak-mehta/present).

### Edición de Video y Audio
Cuando los eventos se realizan en línea, algunas presentaciones no se hacen en vivo, el conferencista tiene que grabar su presentación previo al evento.

Especialmente durante la pandemia, fui conferencista en muchos eventos virtuales, y tuve que grabar algunas presentaciones antes del evento. Las herramientas que usé incluían:

* [OBS Studio](https://obsproject.com/) - Para grabar
* [Kdenlive](https://kdenlive.org/en/) - Para edición de video
* [Audacity](https://www.audacityteam.org/) - Para edición de audio

## 6. Asegúrate Que Todo Esté Listo Para El Día Importante
Esta es una lista del hardware que puedes requerir para tu presentación:

* Una computadora
* Un adaptador de USB C a HDMI
* Un control remoto para presentaciones
* Una cámara web, cámara digital o smartphone
* Un micrófono
* Audífonos
* Un mouse
* Un teclado
* Un monitor

De la lista anterior, los primeros tres son los que he usado para eventos presenciales, y el resto de la lista es lo que he usado para eventos virtuales.

Esta lista podría cambiar dependiendo de lo que necesitas, pero tienes que asegurarte que todo esté funcionando antes del día de tu presentación.

Para eventos virtuales, debes tener una conexión a Internet estable.

Algunos eventos no proporcionan a sus conferencistas una conexión a Internet, y esto puede afectar tu presentación si los demos que preparaste requieren una conexión a Internet para funcionar.

Si ese es el caso, sugiero grabar un video para mostrar el demo ejecutándose, o configurar un entorno de desarrollo local con todo lo necesario para el demo.

Si estás usando Google Slides o cualquier herramienta en línea para crear tu presentación, no olvides descargar una copia, ya que puede que no hay conexión a Internet en el lugar.

## 7. Está Bien No Tener Todas Las Respuestas
Tú eres el experto en el tema que discutirás en tu presentación, y es la razón por la que te invitaron al evento. Pero tener conocimientos sobre el tema no significa que tienes todas las respuestas, y está bien decirlo.

No tener las respuestas a preguntas específicas es una oportunidad para aprender. Cuando alguien me hizo una pregunta para la que no tenía la respuesta, o si un demo falló, y no pude arreglarlo, después de mi presentación, dediqué tiempo a encontar la respuesta y luego preparé un artículo para compartir la solución con los asistentes al evento. Aprendí algo nuevo y compartí el conocimiento adquirido.

## 8. ¡Diviértete!
Las cosas pueden salir bien o de formas inesperadas, pero si incluso si es parte de tu trabajo, disfruta el momento. Valió la pena todo por lo que has tenido que pasar mientras te preparabas para tu presentación.

He estado compartiendo conocimiento con el mundo por casi 16 años. ¿Por qué continuo haciendolo después de tanto tiempo?

Para responder esta pregunta, aquí están mis razones principales:

* Aprender mucho sobre diferentes temas, y conocer nuevas tecnologías
* Conocer muchas personas de todo el mundo
* Viajar a otros países
* Presentar en inglés y español