---
title: "Docker: Configura un Auxiliar de Credenciales"
date: 2023-12-26
description: "Cómo gestionar credenciales de Docker de manera segura"
summary: "Aprende configurar un auxiliar de credenciales para Docker"
tags: ["tutorial", "docker", "linux"]
---

Si usas Docker desde la línea de comandos, puedes iniciar sesión en tu cuenta de Docker Hub con el siguiente comando:

```
$ docker login
```

Tienes que proporcionar tu usuario y contraseña. Si tienes la autenticación en dos pasos activada, debes crear un token de acceso para usuarlo como contraseña de inicio de sesión.

Los detalles de autenticación se almacenan en el archivo `~/.docker/config.json`.

El archivo almacenará las credenciales en texto plano y se verá así:

```
{
  "auths": {
    "https://index.docker.io/v1/": {
	  "auth": "dXNlcm5hbWU6cGFzc3dvcmQ="
    }
  }
}
```

El archivo usa `base64` para cifrar los detalles de autenticación. Por ejemplo, si descifras el valor de `auth`, obtienes:

```
$ echo -n 'dXNlcm5hbWU6cGFzc3dvcmQ=' | base64 --decode
username:password
```

En ese caso, una forma más segura de manejar tus credenciales será usando un [auxiliar de credenciales](https://github.com/docker/docker-credential-helpers/).

Estos son los programas disponibles que pueden usarse como auxiliar de credenciales:

* `osxkeychain`: Proporciona un auxiliar para usar el gestor de contraseñas de OS X como almacén de credenciales.
* `secretservice`: Proporciona un auxiliar para usar el servicio de secrets de D-Bus cómo almacén de credenciales.
* `wincred`: Proporciona un auxiliar para usar el gestor de credenciales de Windows como almacén.
* `pass`: Proporciona un auxiliar para usar `pass` como almacén de credenciales.

En Linux, puedes usar `secretservice`. Para instalarlo ejecuta los siguientes comandos:

```
wget -O docker-credential-secretservice https://github.com/docker/docker-credential-helpers/releases/download/v0.8.0/docker-credential-secretservice-v0.8.0.linux-amd64
chmod +x docker-credential-secretservice
sudo mv docker-credential-secretservice /usr/local/bin/
```

En seguida, configura la opción `credsStore` en tu archivo `~/.docker/config.json`:

```
sed -i '0,/{/s/{/{\n\t"credsStore": "secretservice",/' ~/.docker/config.json
```

Cierra sesión:

```
docker logout
```

Las credenciales se remueven del archivo `~/.docker/config.json`.

Inicia sesión otra vez:

```
docker login
```

Preguntará por tu usuario y contraseña otra vez, pero en esta ocasión el archivo `~/.docker/config.json` tendrá el siguiente contenido:

```
{
  "auths": {
    "https://index.docker.io/v1/": {}
  },
  "credsStore": "secretservice"
}
```

Cuando ejecutas Docker Desktop, un auxiliar de credenciales es proporcionado, y no necesitas configurar uno manualmente. El archivo `~/.docker/config.json`, en Windows, tendrá el siguiente contenido:

```
{
  "auths": {
	"https://index.docker.io/v1/": {}
  },
  "credsStore": "desktop.exe"
}
```

Ahora puedes gestionar tus credenciales de manera más segura.