---
title: "Experience"
date: 2022-03-08
layout: "simple"
sharingLinks: false
---

## Work
{{< timeline >}}

{{< timelineItem icon="code" header="Infotec" badge="May 2024 - Present" subheader="Developer" >}}
Working on improvements, debugging and implementing new features for the Rizoma platform at the National Council of Humanities, Sciences, and Technologies (CONAHCYT), and supporting data correction and update requests.
{{< keywordList >}}
{{< keyword >}} TypeScript {{< /keyword >}}
{{< keyword >}} Vue.js {{< /keyword >}}
{{< keyword >}} Python {{< /keyword >}}
{{< keyword >}} MongoDB {{< /keyword >}}
{{< keyword >}} PostgreSQL {{< /keyword >}}
{{< /keywordList >}}
{{< /timelineItem >}}

{{< timelineItem icon="code" header="Percona" badge="July 2022 - May 2023" subheader="Technical Evangelist" >}}
<ul>
  <li>Technical Writing (<a href="https://percona.com">percona.com</a>/<a href="https://percona.community">percona.community</a>) &rarr; <a href="https://mariog.dev/tags/percona/">Blog Posts</a></li>
  <li>Technical Documentation (Improvement)</li>
  <li>Live Stream Host/Co-host (MySQL/PostgreSQL) &rarr; <a href="https://percona.community/speakers/mario_garcia/">Recordings</a></li>
  <li>Public Speaking</li>
</ul>
{{< keywordList >}}
{{< keyword >}} Python {{< /keyword >}}
{{< keyword >}} MySQL {{< /keyword >}}
{{< keyword >}} PostgreSQL {{< /keyword >}}
{{< keyword >}} MongoDB {{< /keyword >}}
{{< keyword >}} Docker {{< /keyword >}}
{{< keyword >}} Kubernetes {{< /keyword >}}
{{< keyword >}} AWS {{< /keyword >}}
{{< /keywordList >}}
{{< /timelineItem >}}

{{< timelineItem icon="code" header="TerminusDB" badge="July 2021 - October 2021" subheader="Intern, Community Team" >}}
Tutorials
<ul>
  <li>Dev environment configuration</li>
  <li>Schema creation</li>
  <li>Import data to TerminusDB</li>
  <li>Python client usage</li>
  <li>Scaffolding tool usage</li>
  <li>Singer.io tap and target usage</li>
</ul>
GitHub:
<ul>
  <li><a href="https://github.com/mattdark/terminusdb-tutorials/tree/master/brewery">Brewery</a></li>
  <li><a href="https://github.com/mattdark/terminusdb-tutorials/tree/master/gitlab_data">GitLab</a></li>
  <li><a href="https://github.com/mattdark/terminusdb-tutorials/tree/master/netflix">Netflix</a></li>
  <li><a href="https://github.com/mattdark/terminusdb-tutorials/tree/master/nobel_prize">Nobel Prize</a></li>
</ul>
{{< keyword >}} Python {{< /keyword >}}
{{< /timelineItem >}}
{{< /timeline >}}

## Community Participation
{{< timeline >}}
{{< timelineItem icon="code" header="HashiCorp" badge="2021 - 2023" subheader="HashiCorp Ambassador" >}}
<ul>
  <li>Content creator. Blog posts and videos.</li>
  <li>Evangelism. Speaker & Instructor at Conferences, Meetups and Webinars.</li>
</ul>
{{< /timelineItem >}}

{{< timelineItem icon="code" header="GitLab" badge="August 2019 - Ongoing" subheader="GitLab Hero" >}}
<ul>
  <li>Content creator. Blog posts and videos.</li>
  <li>Evangelism. Speaker & Instructor at Conferences, Meetups and Webinars.</li>
</ul>
{{< /timelineItem >}}

{{< timelineItem icon="code" header="GitKraken" badge="April 2019 - 2022" subheader="GitKraken Ambassador" >}}
<ul>
  <li>Content creator. Blog posts and videos.</li>
  <li>Evangelism. Speaker & Instructor at Conferences, Meetups and Webinars.</li>
</ul>
{{< /timelineItem >}}

{{< timelineItem icon="code" header="Mozilla Foundation" badge="August 2011 - September 2020" subheader="Contributor" >}}
<ul>
  <li>Firefox OS. Launch Team for Mexico (2013 – 2015)</li>
  <li>Localization. Firefox & Webmaker. (2012 – 2013)</li>
  <li>Rust. Speaker & Instructor. (2016 – 2020)</li>
  <li>Open Leaders. Featured Expert & Mentor (February 2018 – December 2020)</li>
  <li>Mozilla Reps (August 2017 – August 2020)</li>
</ul>
{{< /timelineItem >}}

{{< /timeline >}}