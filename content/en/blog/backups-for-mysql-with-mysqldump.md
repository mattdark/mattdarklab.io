---
title: "Backups for MySQL With mysqldump"
date: 2023-03-10T14:06:54-06:00
externalUrl: "https://percona.community/blog/2023/03/10/backups-for-mysql-with-mysqldump/"
summary: ""
tags: ["percona", "mysql", "community"]
showReadingTime: false
_build:
  render: "false"
  list: "local"
---
