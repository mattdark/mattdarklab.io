---
title: "MongoDB: How to Convert BSON Documents to a Human-readable Format"
date: 2023-04-03T14:51:32-06:00
externalUrl: "https://www.percona.com/blog/mongodb-how-to-convert-bson-documents-to-a-human-readable-format/"
summary: ""
tags: ["percona", "python", "mongodb", "bash"]
showReadingTime: false
_build:
  render: "false"
  list: "local"
---
